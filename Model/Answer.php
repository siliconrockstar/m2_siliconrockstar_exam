<?php

namespace Siliconrockstar\Exam\Model;

class Answer extends \Magento\Framework\Model\AbstractModel implements \Siliconrockstar\Exam\Api\Data\AnswerInterface, \Magento\Framework\DataObject\IdentityInterface
{

    const CACHE_TAG = 'siliconrockstar_exam_answer';

    protected function _construct() {
        $this->_init('Siliconrockstar\Exam\Model\ResourceModel\Answer');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

}
