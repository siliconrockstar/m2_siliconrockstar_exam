<?php

namespace Siliconrockstar\Exam\Model;

class Question extends \Magento\Framework\Model\AbstractModel implements \Siliconrockstar\Exam\Api\Data\QuestionInterface, \Magento\Framework\DataObject\IdentityInterface
{

    const CACHE_TAG = 'siliconrockstar_exam_question';

    public function _construct() {
        $this->_init('Siliconrockstar\Exam\Model\ResourceModel\Question');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
}
