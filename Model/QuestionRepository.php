<?php

namespace Siliconrockstar\Exam\Model;

use Siliconrockstar\Exam\Api\QuestionRepositoryInterface;
use Siliconrockstar\Exam\Api\Data\QuestionInterface;
use Siliconrockstar\Exam\Model\QuestionFactory;
use Siliconrockstar\Exam\Model\ResourceModel\Question as ObjectResourceModel;
use Siliconrockstar\Exam\Model\ResourceModel\Question\CollectionFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\App\ResourceConnection;

class QuestionRepository implements QuestionRepositoryInterface
{

    protected $objectFactory;
    protected $objectResourceModel;
    protected $collectionFactory;
    protected $searchResultsFactory;
    protected $dbResourceConnection;

    public function __construct(
    QuestionFactory $objectFactory, 
            ObjectResourceModel $objectResourceModel, 
            CollectionFactory $collectionFactory, 
            SearchResultsInterfaceFactory $searchResultsFactory, 
            ResourceConnection $resourceConnection
    ) {
        $this->objectFactory = $objectFactory;
        $this->objectResourceModel = $objectResourceModel;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dbResourceConnection = $resourceConnection;
    }

    public function save(QuestionInterface $object) {
        try {
            $this->objectResourceModel->save($object);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $object;
    }

    public function getById($id) {
        $object = $this->objectFactory->create();
        $this->objectResourceModel->load($object, $id);
        if (!$object->getId()) {
            throw new NoSuchEntityException(__('Object with id "%1" does not exist.', $id));
        }
        return $object;
    }

    public function delete(QuestionInterface $object) {
        try {
            $this->objectResourceModel->delete($object);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    public function deleteById($id) {
        return $this->delete($this->getById($id));
    }

    public function getList(SearchCriteriaInterface $criteria) {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $collection = $this->collectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];
            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                        $sortOrder->getField(), ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $objects = [];
        foreach ($collection as $objectModel) {
            $objects[] = $objectModel;
        }
        $searchResults->setItems($objects);
        return $searchResults;
    }

    /**
     * huge pain in the ass using ORM layers, just go direct to db
     * 
     * SELECT question_id FROM siliconrockstar_exam_question ORDER BY RAND() LIMIT 1
     */
    public function getRandomQuestionId(): string {

        $cnxn = $this->dbResourceConnection->getConnection();
        $tablename = $cnxn->getTableName('siliconrockstar_exam_question');
        
        $sql = 'SELECT question_id FROM ' . $tablename . ' ORDER BY RAND() LIMIT 1';
        $query = $cnxn->query($sql);
        $query->execute();
        
        $result = $query->fetchAll()[0]['question_id']; 
        
        /* sanity check */
        if (empty($result) || !is_numeric($result)) {
            throw new \Exception('Could not get a random question id');
        } else {
            return (string) $result;
        }
    }

}
