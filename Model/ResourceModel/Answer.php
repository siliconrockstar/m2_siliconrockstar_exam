<?php
namespace Siliconrockstar\Exam\Model\ResourceModel;
class Answer extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('siliconrockstar_exam_answer','answer_id');
    }
}
