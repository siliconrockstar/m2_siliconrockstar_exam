<?php

namespace Siliconrockstar\Exam\Model\ResourceModel\Question;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    public function _construct() {
        $this->_init('Siliconrockstar\Exam\Model\Question', 'Siliconrockstar\Exam\Model\ResourceModel\Question');
    }

}
