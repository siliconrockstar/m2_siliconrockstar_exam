#!/bin/bash

cd ../../../../../ ; 
./bin/magento module:disable Siliconrockstar_Exam;
mysql -u root --password='Password1!' --execute='USE ecs ;
DROP TABLE IF EXISTS siliconrockstar_exam_answer ; 
DROP TABLE IF EXISTS siliconrockstar_exam_question ; 
DELETE FROM setup_module WHERE module="Siliconrockstar_Exam" ; 
DELETE FROM authorization_rule WHERE resource_id LIKE "Siliconrockstar_Exam%" ;
DELETE FROM authorization_role WHERE role_name LIKE "Siliconrockstar%Exam%" ; ';
./bin/magento module:enable Siliconrockstar_Exam;
./bin/magento setup:upgrade;
cd ./siliconrockstar ;
./flush-cache.sh;
