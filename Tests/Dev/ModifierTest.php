<?php

/**
 * unit-style tests that actually use the database, written during development
 * 
 * @author dev
 */

namespace Siliconrockstar\Exam\Test\Dev\Model;

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Http;

require_once __DIR__ . '/../../../../../bootstrap.php';

class DataHelperTest extends \PHPUnit\Framework\TestCase
{

    protected $om;

    public function setUp() {
        Bootstrap::create(BP, $_SERVER)->createApplication(Http::class);
        $this->om = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * run this and check that your log file has records
     */
    public function testDataHelper() {
        
        $modifier = $this->om->getInstance()->create('\Siliconrockstar\Exam\Ui\DataProvider\Question\Form\Modifier');
        
        # umm...
        
    }

}
