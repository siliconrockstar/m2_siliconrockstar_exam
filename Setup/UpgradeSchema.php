<?php

namespace Siliconrockstar\Exam\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {

        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.1.0') < 0) {

            /* create study_guide_section table */
            $sgsTableDefinition = $setup->getConnection()->newTable(
                            $setup->getTable('siliconrockstar_exam_study_guide_section')
                    )->addColumn(
                            'study_guide_section_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Question Id'
                    )->addColumn(
                            'study_guide_section', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 64, [], 'STudy Guide Section'
                    )->setComment(
                    'Questions'
            );
            
            $setup->getConnection()->createTable($sgsTableDefinition);
        }

        $setup->endSetup();
    }

}
