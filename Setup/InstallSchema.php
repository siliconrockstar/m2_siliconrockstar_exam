<?php

namespace Siliconrockstar\Exam\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
    
        $setup->startSetup();
        
        /* create questions table */
        $questionsTableDefinition = $setup->getConnection()->newTable(
                        $setup->getTable('siliconrockstar_exam_question')
                )->addColumn(
                        'question_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Question Id'
                )->addColumn(
                        'question_text', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 5000, [], 'Question text'
                )->addColumn(
                        'study_guide_section', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 20, [], 'Corresponding Study Guide Section'
                )->addColumn(
                        'creation_time', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT,], 'Creation Time'
                )->addColumn(
                        'update_time', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE,], 'Modification Time'
                )->addColumn(
                    'is_active', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '1',], 'Is Active'
                )->setComment(
                    'Questions'
        );
        $setup->getConnection()->createTable($questionsTableDefinition);

        /* create answers table */
        $answersTableDefinition = $setup->getConnection()->newTable(
                        $setup->getTable('siliconrockstar_exam_answer')
                )->addColumn(
                        'answer_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Answer Id'
                )->addColumn(
                        'question_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Question Id of related question'
                )->addColumn(
                        'answer_text', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 2000, [], 'Answer text'
                )->addColumn(
                        'is_correct', \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN, null, [], 'Answer is a correct answer'
                )->addColumn(
                        'creation_time', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT,], 'Creation Time'
                )->addColumn(
                        'update_time', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE,], 'Modification Time'
                )->addColumn(
                    'is_active', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '1',], 'Is Active'
                )->setComment('Answers');
        $setup->getConnection()->createTable($answersTableDefinition);

        /* add fk to answers table to reference parent question */
        /**
         * @see Magento\Framework\Setup\SchemaSetupInterface
         * public function getFkName($priTableName, $priColumnName, $refTableName, $refColumnName); 
         */
        $fkName = $setup->getFkName(
                'siliconrockstar_exam_answer', 'question_id', 'siliconrockstar_exam_question', 'question_id'
        );
        /**
         * @see Magento\Framework\DB\Adapter\Pdo\Mysql
         */
        $setup->getConnection()->addForeignKey(
                $fkName, 'siliconrockstar_exam_answer', 'question_id', 'siliconrockstar_exam_question', 'question_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $setup->endSetup();
    }

}
