<?php

$qaData = array(
    /*
      What folder should your module's observers live in?
      ./Event
      > ./Observer
     */
    array(
        'question_text' => 'Objects which are extendable by extension attributes implement which interface?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'ExtensibleDataInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'ExtensionAttributeInterface',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'EavInterface',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'DataExtensionInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Automatically generated events like *_save_before and *_load_after are implemented in what class?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Framework\Model\AbstractModel',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Framework\DataObject',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Magento\Framework\EavInterface',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'DataExtensionInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Data getters/setters are implemented in what class?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Framework\DataObject',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Framework\Model\DataObject',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Magento\Framework\Model\AbstractModel',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Varien\Db',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which two modules contain the models that use EAV attributes most heavily?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'Customer',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Catalog',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Checkout',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Data',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
