<?php

$qaData = array(
    array(
        'question_text' => 'Catalog rules and the products they apply to are not indexed.',
        'study_guide_section' => '7.4',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Catalog rules can be used to ',
        'study_guide_section' => '7.4',
        'answers' => array(
            array(
                'answer_text' => 'discount products with a certain attribute set',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'discount certain categories of products',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'grant a discount based on cart total',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'grant a discount based on shipping method',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Catalog rules ___ be restricted to certain user groups; cart rules ___ be restricted to certain user groups.',
        'study_guide_section' => '7.4',
        'answers' => array(
            array(
                'answer_text' => 'can, can',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'cannot, cannot',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'can, cannot',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'cannot, can',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The simplest way to set a discount for a single product is to ',
        'study_guide_section' => '7.4',
        'answers' => array(
            array(
                'answer_text' => 'use that product\'s special price',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'create a catalog rule for that single product',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Unlike special prices, catalog rules can ',
        'study_guide_section' => '7.4',
        'answers' => array(
            array(
                'answer_text' => 'apply to only certain customer groups',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'be applied to multiple products easily',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Catalog rules are indexed by the Product Price indexer.',
        'study_guide_section' => '7.4',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Catalog rules and associated products are indexed by the Catalog Rule Product indexer.',
        'study_guide_section' => '7.4',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The catalog rule and product price index uses what table?',
        'study_guide_section' => '7.4',
        'answers' => array(
            array(
                'answer_text' => 'catalogrule_product_price',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'catalog_price',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A helpful step to debugging catalog rules it to ',
        'study_guide_section' => '7.4',
        'answers' => array(
            array(
                'answer_text' => 'check the data in the catalogrule_product_price table',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'iterate the product\'s attributes',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
