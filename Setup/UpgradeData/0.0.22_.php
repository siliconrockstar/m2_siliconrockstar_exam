<?php

$qaData = array(
    array(
        'question_text' => 'XML layout operates as a link between ___ and ___.',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'blocks, templates',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'blocks, models',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'models, templates',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The <action> layout element is deprecated.',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The <block> layout element ',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'creates a new block',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'references an existing block',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The <container> layout element ',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'creates a new grouping of blocks',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'references an existing page',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The <container> layout element allows you to specify the html tag and html class that wraps the child blocks.',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The class attribute is required for <block>s.',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The class attribute is no longer required for blocks in Magento > 2.2.1',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'If a class is not specified for a block, the default class of ___ is used.',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Framework\View\Element\Template',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\App\Block\Element\Template',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'All blocks are cacheable by default.',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Specifying cacheable="false" on a block disables full page cache for the whole page.',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How do you remove a block using layout?',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'set its remove attribute = true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'use the <remove> tag',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The <remove> tag is only used in the <body> section of the page',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The <remove> tag is only used to remove static assets from within <head>',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What does <update> do?',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'include a certain layout handle',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'update the parent layout node',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'An <argument> tag must always be enclosed by ',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => '<arguments>',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<body>',
                'is_correct' => false
            ),
            array(
                'answer_text' => '<type>',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In a layout file, the htmlTag attribute ',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'lets you specify what html tag to wrap the elements output in',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'allows you to inject ad hoc html elements',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'If you do not specify a value for a block\'s name attribute, a random name is generated in the format ANONYMOUS_n',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The before and after attributes of blocks operate ',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'within the context of the block\'s parent node',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'within the context of the page',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Setting the before attribute to ___ makes the block appear before all siblings.',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => '-',
                'is_correct' => true
            ),
            array(
                'answer_text' => '0',
                'is_correct' => false
            ),
            array(
                'answer_text' => '+',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You pass variables from layout to block using ',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => '<arguments> and <argument>',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<value>',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'the variable name from the block constructor',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'An <argument> should have which attributes defined?',
        'study_guide_section' => '3.3',
        'answers' => array(
            array(
                'answer_text' => 'name',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'xsi:type',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'scope',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'value',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
