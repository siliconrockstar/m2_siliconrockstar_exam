<?php

$qaData = array(
    array(
        'question_text' => 'What of these paths are functional http application entry points?',
        'study_guide_section' => '2.1l',
        'answers' => array(
            array(
                'answer_text' => './index.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => './pub/index.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => './var/index.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => './www/index.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the recommended http application entry point?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => './index.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => './pub/index.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => './var/index.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => './www/index.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which file is the public media files entry point?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => './pub/index.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => './pub/get.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => './var/index.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => './pub/static.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which file is the entry point for static resources like css and js?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => './pub/index.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => './pub/static.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => './var/index.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => './www/static.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which file does a require_once on the autoloader?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => './app/bootstrap.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => './pub/static.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => './pub/index.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => './www/static.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which controller class does the application use if area = webapi_rest?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Webapi\Controller\Rest',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Controller\Rest',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which controller class does the application use if area = webapi_soap?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Webapi\Controller\Soap',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Soap',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which controller class does the application use if area = frontend?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framwork\App\Http',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Webapi\Controller\Http',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which class iterates the list of routers and tries to find a match for the http request\'s route?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framwork\App\FrontController',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Webapi\Controller\Http',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\App',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What interface must all controllers implement?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framwork\App\ActionInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Webapi\Controller\Http',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\App\ControllerInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What interface do routers implement?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framwork\App\RouterInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Webapi\Controller\Router',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\App\ControllerInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What public method must all front controllers implement?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => 'execute()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'run()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'route()',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What class/file boostraps the ObjectManager in its constructor?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\App\Boostrap',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\App',
                'is_correct' => false
            ),
            array(
                'answer_text' => './app/bootstrap.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the earliest event the M2 frontend dispatches?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => 'controller_action_predispatch',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'router_match',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'application_route',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What event can you hook into if you want to always catch the complete response data?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => 'controller_action_postdispatch',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'router_done',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'application_preresponse',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How do you disable maintenance mode?',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => './bin/magento maintenance:disable',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'rm ./maintenance.flag',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => '\Magento\Framework\App\FrontControllerInterface is involved in every non-API http request',
        'study_guide_section' => '2.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento determines the area based on the frontname in the URL',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
