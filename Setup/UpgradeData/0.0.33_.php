<?php

$qaData = array(
    array(
        'question_text' => 'How can you create a category?',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => 'in admin',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'in code, using \Magento\Catalog\Api\Data\CategoryInterfaceFactory',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Categories support extension attributes.',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Categories support extension attributes.',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Categories are an EAV datatype.',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The primary category table in the database is ',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => 'catalog_category_entity',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'catalog_category',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'core_category',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The primary category table in the database is ',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => 'catalog_category_entity',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'catalog_category',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'core_category',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In the category hierarchy, categories can have ___ parent(s).',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => 'one',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'multiple',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A category\'s hierarchy is stored in which column in the db?',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => 'path',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'position',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'level',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Category hierarchy paths are constructed by ',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => 'concatenating the entity_id and each parent category entity_id with a /',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'concatenating each parent category entity_id with a :',
                'is_correct' => false
            ),
        )
    ),
    array(
        'question_text' => 'If a category has path 4/5/7, then the category\'s entity_id is',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => '7',
                'is_correct' => true
            ),
            array(
                'answer_text' => '4',
                'is_correct' => false
            ),
        )
    ),
    array(
        'question_text' => 'Which attribute must you add to a new category to create it in admin?',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => 'name',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'parent',
                'is_correct' => false
            ),
        )
    ),
    array(
        'question_text' => 'A category with path 2/5/8/7 would have a level of ',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => '3',
                'is_correct' => true
            ),
            array(
                'answer_text' => '4',
                'is_correct' => false
            ),
            array(
                'answer_text' => '2',
                'is_correct' => false
            ),
        )
    ),
    array(
        'question_text' => 'The category with id 0 exists in ___ but does not exist in ___',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => 'code, the database',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'teh database, code',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The tree root category has entity_id ',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => '1',
                'is_correct' => true
            ),
            array(
                'answer_text' => '0',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The category with id 1 ___ visible in admin and ___ visible on the frontend',
        'study_guide_section' => '7.3',
        'answers' => array(
            array(
                'answer_text' => 'is, is not',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'is not, is',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'is not, is not',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'is, is',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
