<?php

$qaData = array(
    array(
        'question_text' => 'Which of the following support extension attributes?',
        'study_guide_section' => '8.1',
        'answers' => array(
            array(
                'answer_text' => 'quote',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'quote item',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'address',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'shopping cart rule',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'cart',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'cart item',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'checkout',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Quotes are not associated with a customer session.',
        'study_guide_section' => '8.1',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => '___ are converted into ___',
        'study_guide_section' => '8.1',
        'answers' => array(
            array(
                'answer_text' => 'quotes, orders',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'orders, quotes',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Quote items ',
        'study_guide_section' => '8.1',
        'answers' => array(
            array(
                'answer_text' => 'link the customer\'s shopping cart to products',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'contain the cart totals',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'are stored in the quote_item table',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'are stored in the quote table',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Quotes ',
        'study_guide_section' => '8.1',
        'answers' => array(
            array(
                'answer_text' => 'link the customer\'s shopping cart to products',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'contain the cart totals',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'are stored in the quote_item table',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'are stored in the quote table',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Shopping cart rules are implemented by the ___ module',
        'study_guide_section' => '8.1',
        'answers' => array(
            array(
                'answer_text' => 'SalesRule',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'CartRule',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Checkout',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When customizing adding a product to the cart, you may need to account for which scenarios?',
        'study_guide_section' => '8.1',
        'answers' => array(
            array(
                'answer_text' => 'a product is added to the cart on teh frontend',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'a product is added to a cart in admin via "Create Order"',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'a product is added to a cart from a wishlist',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'a product is reordered',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'a customer logs in from a different machine and cart items are merged',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'a product is added to cart via the API',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Generally speaking, when customizing add to cart globally, you should ',
        'study_guide_section' => '8.1',
        'answers' => array(
            array(
                'answer_text' => 'use plugins around functions that are triggered in every add to cart scenario',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'hook in to events that are trigged in specific add to cart scenarios',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Quotes are EAV objects.',
        'study_guide_section' => '8.1',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true ',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
