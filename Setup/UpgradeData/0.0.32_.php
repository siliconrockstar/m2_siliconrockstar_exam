<?php

$qaData = array(
    array(
        'question_text' => 'The main price calculation function is ',
        'study_guide_section' => '7.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Catalog\Model\Product\Type\Price::calculatePrice',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Catalog\Model\Product::calculatePrice',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Price calculation\'s initial starting point is ',
        'study_guide_section' => '7.1',
        'answers' => array(
            array(
                'answer_text' => 'the product\'s base price',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the product\'s special price',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In the case of a simple product with quantity=1, to determine the price, Magento takes into account ',
        'study_guide_section' => '7.1',
        'answers' => array(
            array(
                'answer_text' => 'base price',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'special pricing',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'catalog rules',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'tiered pricing',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'options price',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'VAT',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What might Magento account for when calculating price for a configurable product with quantity greater than 1, '
        . 'that it would not consider when calculating price for a simple product of quantity=1?',
        'study_guide_section' => '7.1',
        'answers' => array(
            array(
                'answer_text' => 'base price',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'special pricing',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'catalog rules',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'tiered pricing',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'options prices',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'VAT',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'In di.xml, use ___ when you want to replace a class globally, but ___ when you want to replace a class only within a certain scope.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'preference, argument',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'argument, preference',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Prices a rendered by a block of class ___, or one of its descendents.',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Pricing\Render',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Catalog\Pricing\Render',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the name of the default block can you insert into layout to render price?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => 'product.price.render.default',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'catalog.price',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can render price in a page by using ___ to insert a block of type ___.',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => 'layout, \Magento\Framework\Pricing\Render',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'di.xml, \Magento\Catalog\Pricing\Render',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When rendering price via layout, your block might be passed what arguments?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => 'price_render',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'price_type_code',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'product_type',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Pricing templates can be found at',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => '/vendor/magento/module-catalog/view/base/templates/product/price/*.phtml',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/vendor/magento/module-price/view/base/templates/product/price/*.phtml',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/vendor/magento/module-catalog/view/base/web/template/product/*.html',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/vendor/magento/module-catalog/view/base/ui_component/template/*.html',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
