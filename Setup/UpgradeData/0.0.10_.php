<?php

$qaData = array(
    array(
        'question_text' => 'Where would you expect to find modules?',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => '/app/code/CompanyName/ModuleName',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/vendor/vendor-name/module-name',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/lib/CompanyName/ModuleName',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/app/design/area/CompanyName/ModuleName',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The modules in /vendor are installed by ___',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => 'Composer',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento Core',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Jenkins',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'module setup scripts',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Modules in /vendor ___ be edited directly',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => 'should not',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'should',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these files correctly specifies that module Company_Widget depends on Magento_Catalog?',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => '<?xml version="1.0"?>
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:Module/etc/module.xsd">
    <module name="Company_Widget" setup_version="0.0.4">
        <sequence>
            <module name="Magento_Catalog"/>
        </sequence>
    </module>
</config>
',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<?xml version="1.0"?>
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:Module/etc/module.xsd">
    <module name="Company_Widget" setup_version="0.0.4">
        <after>
            <module name="Magento_Catalog"/>
        </after>
    </module>
</config>',
                'is_correct' => false
            ),
            array(
                'answer_text' => '<?xml version="1.0"?>
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:Module/etc/module.xsd">
    <module name="Company_Widget" setup_version="0.0.4">
        <depends>
            <module name="Magento_Catalog"/>
        </depends>
    </module>
</config>',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What module file specifies loading sequence and setup version?',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => './etc/module.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/config.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/setup.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Disabling a module ___ remove its tables from the database.',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => 'does not',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'does',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'M2 ___ PSR-4 compliant.',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => 'is',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'is not',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'It is preferable to have your module depend on external ___ instead of ___',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => 'interfaces, classes',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'classes, interfaces',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In what directories might M2 core files be found in?',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => '/vendor/magento, if you installed using composer',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/app/code/Magento, if you are contributing to core',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/lib, for supporting JS and CSS',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/skin, for frontend themes',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/app/design/adminthml, for admin area theme',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Since M2.2.1, you can avoid creating a block class as long as you declare the block in layout and include what?',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => 'a value for the template attribute, and an argument for the view_model parameter',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'an argument for the template',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/lib, for supporting JS and CSS',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'ViewModels that are injected into blocks must implement what interface?',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Framework\View\Element\Block\ArgumentInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Framework\View\Element\ViewModel\ArgumentInterface',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Magento\Framework\Block\ArgumentInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Where should commands for the Magento binary reside in your module?',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => './Console',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Bin',
                'is_correct' => false
            ),
            array(
                'answer_text' => './shell',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento expects to find controllers in what directory in your module?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './Controller',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Controllers',
                'is_correct' => false
            ),
            array(
                'answer_text' => './controller',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In M2, the data helper at ./Helper/Data.php ___ required.',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'is no longer',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'is still',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Where are the classes that handle database interactions within your module?',
        'study_guide_section' => '1.2',
        'answers' => array(
            array(
                'answer_text' => './Model/ResourceModel',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Resource',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Crud',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Event listeners go in what directory within your module?',
        'study_guide_section' => '1.2',
        'answers' => array(
            array(
                'answer_text' => './Observer',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Listeners',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Event',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'UI component data providers are stored where in your module?',
        'study_guide_section' => '1.2',
        'answers' => array(
            array(
                'answer_text' => './Ui',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Component',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Api/Data',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Block templates are stored where in your module?',
        'study_guide_section' => '1.2',
        'answers' => array(
            array(
                'answer_text' => './view/(area)/templates',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/(area)/template',
                'is_correct' => false
            ),
            array(
                'answer_text' => './view/templates',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Block/templates',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What does ./view/adminhtml/ui_component contain?',
        'study_guide_section' => '1.2',
        'answers' => array(
            array(
                'answer_text' => 'XML to configure admin area UI components',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'PHP data provider classes for admin area UI component',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'JS for admin area UI components',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is stored at ./view/(area)/web/template?',
        'study_guide_section' => '1.2',
        'answers' => array(
            array(
                'answer_text' => 'phtml templates for use with Blocks',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'html templates for use with KnockoutJS',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Where is Require.js configuration stored in your module?',
        'study_guide_section' => '1.2',
        'answers' => array(
            array(
                'answer_text' => './view/(area)/requirejs-config.js',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/(area)/web/config.js',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What file type should you *not* find in ./view/(area)/web?',
        'study_guide_section' => '1.2',
        'answers' => array(
            array(
                'answer_text' => '.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => '.js',
                'is_correct' => false
            ),
            array(
                'answer_text' => '.html',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento 2 allows you to specify layout xml on a per-area basis.',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which layout file is required in your module\'s ./etc folder?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => 'module.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'config.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'config.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which file defines premissions for accessing protected admin resources?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => './etc/acl.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'config.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'config.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the naming convention for layout files?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => 'routeid_controllerfrontname_controlleraction.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'controllerfrontname_controlleraction.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'routerid_controllerfrontname_controlleraction.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What file loads the default configuration values into Stores > Configuration?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => './etc/config.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/adminhtml/system.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/module.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In what file would you specify a field in Stores > Configuration should be encrypted?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => './etc/config.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/adminhtml/system.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/module.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Virtual types are declared in what file?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => './etc/di.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/config.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/module.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Where in your module do you store email templates?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => './view/frontend/email',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/email',
                'is_correct' => false
            ),
            array(
                'answer_text' => './view/email',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Where are indexers declared?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => './etc/indexer.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/index.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './bin/indexer.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Admin menu changes are declared in what file?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => './etc/adminhtml/menu.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/menu.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './view/frontend/adminhtml/menu.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the purpose of ./etc/mview.xml?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => 'Allows tracking database changes for a certain entity and running a handler when changes happen.',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Allows configuring display modifications for mobile devices',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => './etc/mview.xml is most often used to implement what?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => 'Modifications to indexing',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Modifications to mysql table views',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Modifications to site behavior for mobile devices',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The "mview" in ./etc/mview.xml is short for what?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => 'Materialized View',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Mobile View',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Massive Victory In Every Way',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What file configures the frontname in URLs for a module?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => './etc/(optional area)/routes.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/etc/front.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'classes in ./Controller',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What does ./etc/view.xml do in a module?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => 'specifies default values for design configuration',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Allows you to override block positions globally',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which files configures api access and routes?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => './etc/webapi.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Data/Api.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/api.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is a widget in M2? (not a jQuery widget)',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'A configurable component that lets you add functionality to CMS pages and blocks',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'A bowl of soup',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In what file are widgets configured?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => './etc/widget.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './widgets.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How would you limit an xml configuration file\'s scope to only the admin area?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => 'put it at ./etc/adminhtml/file.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'add an entry to ./etc/acl.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Can you create custom xml layout/configuration files?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => 'yes',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'no',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What does an .xsd file do?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => 'defines an xml schema',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'transforms an xml file',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What does ./etc/product_types.xml do?',
        'study_guide_section' => '1.3',
        'answers' => array(
            array(
                'answer_text' => 'define and configure product types and their attributes',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'nothing, that file is a lazy good-for-nothing',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
