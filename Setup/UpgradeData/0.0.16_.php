<?php

$qaData = array(
    array(
        'question_text' => 'What is the first part of a Magento URL?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'frontname',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'controller/action path',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'controller/action name',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When it comes to URL routing, the term "controller" and "action" are roughly equivalent.',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the second part of a Magento URL?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'frontname',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'controller/action path',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'controller/action name',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the third part of a Magento URL?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'frontname',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'controller/action path',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'controller/action name',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What is the third part of a Magento URL?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'frontname',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'controller/action path',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'controller/action name',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What are the fourth+ parts of a Magento URL?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'parameters as alternating key/value pairs',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'controller/action path',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'controller/action name',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'list of all parameter names followed by a list of all parameter values',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'To create a custom router, you need to register it where?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => './etc/di.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/config.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/module.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Routers must implement ___ and return an instance of ___.',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\App\RouterInterface, \Magento\Framework\App\ActionInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\App\ActionInterface, \Magento\Framework\App\RouterInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Like the CMS router, a custom router will usually ',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'return an action instance',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'mutate and forward the request to the base router',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'The purpose of URL rewrites is ',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'to make URLs more readable by both humans and search engines',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'to mangle the requested URL and avoid having to declare a custom router',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'URL rewrites are stored in what table?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'url_rewrite',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'url',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'rewrite',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'mod_rewrite',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'URL rewrites are implemented in which module(s)?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'module-catalog-url-rewrite',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'mod-rewrite',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'module-url-rewrite',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'module-request-rewrite',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The URL rewrite module contains a router than checks the url_rewrite table and redirects to the new route if found.',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'User-friendly URLs are stored in an attribute.',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'User-friendly URLs are stored in which attribute.',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'url_key',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'url',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'url_rewrite',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento generated user-friendly URLs by ',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'creating a path based on the category tree and appending the products url identifer',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'something else',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Action controllers usually extend what?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Framework\App\Action\Action',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Framework\App\ActionInterface',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Magento\Framework\App\Action',
                'is_correct' => false
            ),
        )
    ),
    array(
        'question_text' => 'A controller\'s execute() method must return one of which of the following',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Controller\ResultInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\App\ResponseInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Framework\App\Action',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these is deprecated?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Controller\ResultInterface',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\App\ResponseInterface',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Which of these is for rendering html or json?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Controller\ResultInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\App\ResponseInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these is for returning raw output?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Controller\ResultInterface',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\App\ResponseInterface',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Controllers can ',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'forward to anouther route',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'redirect to another URL',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Controller\ResultInterface',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'To forward to another route, a controller returns a ',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => ' \Magento\Framework\Controller\Result\Forward',
                'is_correct' => true
            ),
            array(
                'answer_text' => ' \Magento\Framework\App\Forward',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'To forward to another URL, a controller returns a ',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => ' \Magento\Framework\Controller\Result\Redirect',
                'is_correct' => true
            ),
            array(
                'answer_text' => ' \Magento\Framework\App\Result\Redirect',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When a controller forwards to another URL, the browser receives an http code ',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => '30x',
                'is_correct' => true
            ),
            array(
                'answer_text' => '200',
                'is_correct' => false
            ),
            array(
                'answer_text' => '404',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'All application responses must implement ',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Controller\ResultInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\App\ResponseInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these are valid (default) response types? ',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Controller\Result\Json',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Controller\Result\Forward',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Controller\Result\Raw',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\View\Result\Page',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Controller\Result\Redirect',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'How do you return a response?',
        'study_guide_section' => '2.2',
        'answers' => array(
            array(
                'answer_text' => 'Inject the response\'s factory into your controller, create an instance of the response, set the values needed, and return it',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Instantiate the response via the ObjectManager, set the response data, and return it',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Set values on the global application response object and call sendResponse()',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
