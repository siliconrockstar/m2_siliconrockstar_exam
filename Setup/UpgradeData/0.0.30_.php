<?php

$qaData = array(
    array(
        'question_text' => 'Store configuration (that shows up in the admin area at Stores > Configuration) is configured in your module at ',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => './etc/adminhtml/system.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/admin.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/system.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Stores > Configuration values are stored in which database table?',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => 'core_config_data',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'core_adminhtml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'none, they are written to the filesystem at ./etc/config.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The root node of system.xml is ',
        'study_guide_section' => '6.2',
        'answers' => array(
            array(
                'answer_text' => '<config>',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<system>',
                'is_correct' => false
            ),
            array(
                'answer_text' => '<page>',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The second node of system.xml is ',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => '<config>',
                'is_correct' => false
            ),
            array(
                'answer_text' => '<system>',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<page>',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'system.xml <tab> elements correspond to ',
        'study_guide_section' => '6.2',
        'answers' => array(
            array(
                'answer_text' => 'the highest level groups in the Stores > Configuration screen sidebar - General, Customers, Sales, etc',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'secondary items that exist within the groups in the Stores > Configuration screen sidebar - like "Web" under the Genearal group',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'tertiary items in Stores > Config screen, taht show up in the main content area - like "Url Options" under General > Web',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'html inputs in Stores > Config screen, that show up in the main content area - like "Add Store Code to Urls" under General > Web > Url Options',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'system.xml <section> elements correspond to ',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => 'the highest level groups in the Stores > Configuration screen sidebar - General, Customers, Sales, etc',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'secondary items that exist within the groups in the Stores > Configuration screen sidebar - like "Web" under the Genearal group',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'tertiary items in Stores > Config screen, taht show up in the main content area - like "Url Options" under General > Web',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'html inputs in Stores > Config screen, that show up in the main content area - like "Add Store Code to Urls" under General > Web > Url Options',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'system.xml <group> elements correspond to ',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => 'the highest level groups in the Stores > Configuration screen sidebar - General, Customers, Sales, etc',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'secondary groups that exist within the groups in the Stores > Configuration screen sidebar - like "Web" under the Genearal group',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'tertiary groups in Stores > Config screen, that show up in the main content area - like "Url Options" under General > Web',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'html inputs in Stores > Config screen, that show up in the main content area - like "Add Store Code to Urls" under General > Web > Url Options',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'system.xml <field> elements correspond to ',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => 'the highest level groups in the Stores > Configuration screen sidebar - General, Customers, Sales, etc',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'secondary groups that exist within the groups in the Stores > Configuration screen sidebar - like "Web" under the Genearal group',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'tertiary groups in Stores > Config screen, that show up in the main content area - like "Url Options" under General > Web',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'html inputs in Stores > Config screen, that show up in the main content area - like "Add Store Code to Urls" under General > Web > Url Options',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Scope modifying attributes - like showInDefault, showInWebsite, and showInStore - can be applied to which node types in system.xml?',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => '<tab>',
                'is_correct' => false
            ),
            array(
                'answer_text' => '<section>',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<group>',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<field>',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'system.xml config values are stored in the database by concatenating a path using which attribute\'s value?',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => 'id',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'name',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'group',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'field',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can access control a <section> in system.xml by ',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => 'specifying an ACL resource by creating a child <resource> node',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'specifying an ACL resource using a resource attribute',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In system.xml, a <field> html form type is determined by ',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => 'the value of its type attribute',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'teh value of its <type> child node',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In system.xml, a <field>s type is not required if you specify ',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => 'its frontend model',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'its backend model',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'its template',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In system.xml, a <field>s visibility can be made dependent on another field by ',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => 'using a <depends> child tag',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'using a depends attribute',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'using a visible attribute',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In system.xml, a dropdown\'s list of options can be provided by ',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => 'creating a <field> tag with a <source_model> child tag',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'creating a <section> tag with a source_model attribute',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => '___ stores the default values for store configuration, whereas ___ stores the current values.',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => './etc/config.xml, the core_config_data table',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the core_config_data table, ./etc/config.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/system.xml, the core_config_data table',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can retrieve store configuration values by ',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => 'injecting \Magento\Framework\App\Config\ScopeConfigInterface and calling getValue with a path',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'some other thing',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Store config values are queried by passing a path like ',
        'study_guide_section' => '6.3',
        'answers' => array(
            array(
                'answer_text' => 'general/store_information/name',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'general::store_information::name',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Admin area left sidebar menu configuration is stored in ',
        'study_guide_section' => '6.4',
        'answers' => array(
            array(
                'answer_text' => './etc/adminhtml/menu.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'general::store_information::name',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'menu.xml root and secondary elements are ',
        'study_guide_section' => '6.4',
        'answers' => array(
            array(
                'answer_text' => '<config><menu>',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<config><system>',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these are valid menu.xml directives?',
        'study_guide_section' => '6.4',
        'answers' => array(
            array(
                'answer_text' => '<add>',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<remove>',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<update>',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'If no value for the "parent" attribute is specified in an <add> directive in menu.xml, the item appears ',
        'study_guide_section' => '6.4',
        'answers' => array(
            array(
                'answer_text' => 'as a main sidebar item',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'under the system sidebar entry',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which ACL attribute is used to lookup an ACL?',
        'study_guide_section' => '6.4',
        'answers' => array(
            array(
                'answer_text' => 'id',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'name',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'type',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which menu item attribute is used to restrict an item based on ACL?',
        'study_guide_section' => '6.4',
        'answers' => array(
            array(
                'answer_text' => 'resoure',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'name',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'id',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Admin privileges are allocated to ',
        'study_guide_section' => '6.4',
        'answers' => array(
            array(
                'answer_text' => 'roles',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'users',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Users can have ___ role(s).',
        'study_guide_section' => '6.4',
        'answers' => array(
            array(
                'answer_text' => 'a single',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'multiple',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Roles can have ___ user(s).',
        'study_guide_section' => '6.4',
        'answers' => array(
            array(
                'answer_text' => 'multiple',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'a single',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
