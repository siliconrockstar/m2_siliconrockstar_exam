<?php

$qaData = array(
    array(
        'question_text' => 'Which classes could help you load a list of products based on their type?',
        'study_guide_section' => '7.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Catalog\Api\ProductRepositoryInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Api\SearchCriteriaBuilder',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\App\Config\ConfigSourceInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What class constants might help you filter by a product\'s type?',
        'study_guide_section' => '7.1',
        'answers' => array(
            array(
                'answer_text' => 'TYPE_CODE',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'TYPE_ID',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'PRODUCT_TYPE',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'PRODUCT_TYPE_ID',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What product type is available in commerce edition but not open source?',
        'study_guide_section' => '7.1',
        'answers' => array(
            array(
                'answer_text' => 'gift card',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'subscription',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Some product type configuration can be done in ',
        'study_guide_section' => '7.1',
        'answers' => array(
            array(
                'answer_text' => './etc/product_types.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/product/config.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What responsibilities does the product type model handle?',
        'study_guide_section' => '7.1',
        'answers' => array(
            array(
                'answer_text' => 'loading child products, if applicable',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'preparing the product to be added to the cart',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'checking if the product is saleable',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'loading and configuring product options',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What product type represents the basic unit of inventory?',
        'study_guide_section' => '7.1',
        'answers' => array(
            array(
                'answer_text' => 'simple',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'virtual',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => '___ products are a set of simple products; ___ products are a set of configurable products and their options that constitute the final "build" of a product.',
        'study_guide_section' => '7.1',
        'answers' => array(
            array(
                'answer_text' => 'grouped, bundled',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'bundled, grouped',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => '___ products are essentially ___ products with the ability to ___',
        'study_guide_section' => '7.1',
        'answers' => array(
            array(
                'answer_text' => 'Downloadable, virtual, download them',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Grouped, simple, configure them',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
