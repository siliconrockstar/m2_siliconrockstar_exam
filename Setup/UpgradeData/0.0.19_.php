<?php

$qaData = array(
    array(
        'question_text' => 'If your theme has an ./etc/view.xml file, it is merged with the parent theme\'s ./etc/view.xml file.',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What would you need to do to extend the system to implement a totally custom URL path?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'Create a router class that implements \Magento\Framework\App\RouterInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'In di.xml, add your router to \Magento\Framework\App\RouterList\'s routerList parameter',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'In routes.xml, add your router to \Magento\Framework\App\RouterList\'s routerList parameter',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Create a router class that implements \Magento\Framework\Router\RouterInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What would you need to do to extend the system to implement a totally custom URL path?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'Create a router class that implements \Magento\Framework\App\RouterInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'In di.xml, add your router to \Magento\Framework\App\RouterList\'s routerList parameter',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'In routes.xml, add your router to \Magento\Framework\App\RouterList\'s routerList parameter',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Create a router class that implements \Magento\Framework\Router\RouterInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What do you need to do to implement a new price model for configurable products?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'Make sure your new price model extends \Magento\Catalog\Model\Product\Type\Price',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Set a value for the priceModel attribute for the configurable product type, in ./product_types.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Extend \Magento\Catalog\Model\Product\Type\Simple',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Create a priceInfo class that implements \Magento\Catalog\Model\Product\PriceModifiernterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What does the getFlatColumns method in an EAV\'s source model do?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'returns an array of the columns that will be added to the EAV model\'s flat table',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'returns list of the source model\'s attribute values',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How do you connect an admin area controller and its ACL?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'set the controller\'s ADMIN_RESOURCE constant to the ACL resource',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'add the ACL resource path to the controller class\'s acl attribute in di.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How do you connect an admin area controller and its ACL?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'set the controller\'s ADMIN_RESOURCE constant to the ACL resource',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'add the ACL resource path to the controller class\'s acl attribute in di.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How would you add a link to the customer account sidebar?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'create customer_account.xml and add a block within the block customer_account_navigation',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'create customer_account_index.xml and add a block within the side container',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What directory should admin UI components be located in?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './view/adminhtml/ui_component/',
                'is_correct' => true
            ),
            array(
                'answer_text' => './adminhtml/component/',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Global records search is available on the frontend.',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Global records search is available in admin',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Global records search ',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'allows you to search across multiple business objects - customers, orders, products - at once',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'allows you to search the cache',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'allows customers to search all of their information at once',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento ACLs are avaialable on both the frontend and backend.',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'URL secrets keys are meant to protect against ',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'CSRF',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'SQL injection',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'XSS',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which layout instruction results in Magento rendering HTML?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'container',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'action',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'referenceContainer',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which layout instruction results in Magento rendering HTML?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'block',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'action',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'referenceBlock',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'RequireJS\'s main benefit is ',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'to improve page load time by managing JS dependencies and lazy loading them',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'to integrate with composer',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'to provide a reusable set of JS UI components',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A UI Component can be inserted into the page using what tag?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => '<script type="text/x-magento-init">',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<script type="text/require-js" module="path/to/module">',
                'is_correct' => false
            ),
            array(
                'answer_text' => '<script type="application/magento" src="path/to/script.js">',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which class is useful for specifying search filters to pass to a repository?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Api\SearchCriteriaBuilder',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Api\SearchFilterBuilder',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\Api\RepositorySearchBuilder',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these must be implemented by a system.xml dropdown control\'s source model?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Data\OptionSourceInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Api\SearchFilterBuilder',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\Api\RepositorySearchBuilder',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Settign cacheable="false" on a block prevents the whole page from being cached.',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Setting a block\'s getCacheLifetime() === null prevents the block from being cached without breaking full page caching. ',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is tiered pricing?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'it lowers the price if the customer purchases over certain quantities',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'it applies different pricing rules per customer tier',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these are default Magento product types?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'grouped',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'bundle',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'configurable',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'simple',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'virtual',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'downloadable',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'kit',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'package',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Both grouped and bundle products are composite product types',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => './etc/frontend/di.xml replaces ./etc/di.xml if they both exist and the user is on the frontend',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => './etc/frontend/di.xml is merged with ./etc/di.xml, with the first taking precedence, if the user is on teh frontend',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento will recongnize and load a new module automatically',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'New modules must be enabled with the binary command module:enable',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The binary command module:enable installs a module\'s database changes',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A module\'s database changes must be manually applied with the binary command setup:upgrade',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'UpgradeSchema.php provides an upgrade() method for every version increment.',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'UpgradeSchema.php and UpgradeData.php each provide one method that must handle all version upgrades.',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'UpgradeSchema.php and UpgradeData.php each provide one method that must handle all version upgrades.',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Data and schema upgrade scripts can manage version by ',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'using PHP\'s version_compare method to compare the $context->getVersion() with the required version.',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'referring to the current version in config.xml vs the database',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can disable an observer in a different module by creating an observer with the same name in ./etc/events.xml and setting disabled="true"',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How do you create a new cache?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'create ./etc/cache.xml and specify the class in teh instance attribute',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'extend \Magento\Framework\Cache\FrontendInterface and inject the class into \Magento\App cache argument in di.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => './etc/frontend/routes.xml contains:' . PHP_EOL .
'<route id="widgetizer" frontName="widg-maker">' . PHP_EOL .
  '<module name="Joeco_Widget" />' . PHP_EOL .
'</route>' . PHP_EOL .
'You have placed a controller in Controller/Index/Maker.php. At what URL would you visit the controller listed above?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'widg-maker/index/maker',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'widgetizer/index/maker',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
