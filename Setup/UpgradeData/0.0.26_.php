<?php

$qaData = array(
    array(
        'question_text' => 'The main eav load and save processes are implemented in ',
        'study_guide_section' => '5.2',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Eav\Model\Entity\AbstractEntity',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Framework\Model\AbstractEntity',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The interface for initial loading and saving of EAV entities is more or less the same as simple models.',
        'study_guide_section' => '5.2',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When loading an EAV entity, all attributes and values associated with it are automatically loaded.',
        'study_guide_section' => '5.2',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Entities with very large numbers of attributes ',
        'study_guide_section' => '5.2',
        'answers' => array(
            array(
                'answer_text' => 'can negatively impact performance',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'always have minimal impact on performance',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The negative impact of having entities with very large numbers of attributes can be mitigated by ',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'enabling flat tables',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'circumventing the ORM system and executing SQL queries directly against the database connection',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The negative impact of having entities with very large numbers of attributes can be mitigated by ',
        'study_guide_section' => '5.2',
        'answers' => array(
            array(
                'answer_text' => 'enabling flat tables',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'loading fewer attributes for an entity by using small attribute sets',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'circumventing the ORM system and executing SQL queries directly against the database connection',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The negative impact of having entities with very large numbers of attributes can be made worse by ',
        'study_guide_section' => '5.2',
        'answers' => array(
            array(
                'answer_text' => 'having a large number of stores and websites',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'enabling clock cache',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
