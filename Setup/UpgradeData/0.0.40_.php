<?php

$qaData = array(
    array(
        'question_text' => 'Customer attributes are supported by',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'only Magento 2 Commerce',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'only Magento 2 Open Source',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'both Magento 2 Commerce and Magento 2 Open Source',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Newly created customer attributes are automatically added to admin forms.',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Newly created customer attributes are automatically added to frontend forms.',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Customer attributes are EAV entities.',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Custom customer attributes can have problems saving if is_system is set to ',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => '1',
                'is_correct' => true
            ),
            array(
                'answer_text' => '0',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The Customer model supports extension attributes.',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Customer groups are supported by',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'only Magento 2 Commerce',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'only Magento 2 Open Source',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'both Magento 2 Commerce and Magento 2 Open Source',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Customer groups can be referenced when creating ',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'cart rules',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'catalog price rules',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'tax rules',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'The USA uses ',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'VAT tax',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'sales tax',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'The UK and European Union use ',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'VAT tax',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'sales tax',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => '___ is usually included in the price whereas ___ is usually added to the final price.',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'VAT, sales tax',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'sales tax, VAT',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Configuring VAT usually involves ',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'creating tax classes for VAT',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'creating store views for each country',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'VAT is charged if both the merchant and customer are located in ___ country(ies).',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'the same',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'different',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
