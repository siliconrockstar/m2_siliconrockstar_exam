<?php

$qaData = array(
    /*
      What folder should your module's observers live in?
      ./Event
      > ./Observer
     */
    array(
        'question_text' => 'What folder should your module\'s observers live in?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './Observer',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Event',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Listener',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Controller/(event name)',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In your module, an observer class should handle ___ event(s)',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'one',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'many',
                'is_correct' => false
            )
        )
    ),
    /*
      What is the main method of an Observer class?
      main
      listen
      observe
      execute
     */
    array(
        'question_text' => 'What is the main method of an Observer class?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'public function main()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'protected function execute()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'public function execute()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'protected function observe()',
                'is_correct' => false
            )
        )
    ),
    /*
      Where are plugin classes stored by default?
      > ./Plugin
      ./etc/plugin
      ./Model/Plugin/
     */
    array(
        'question_text' => 'Where are plugin classes stored by default in your module?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './Plugin',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/plugin',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Model/Plugin/',
                'is_correct' => false
            )
        )
    ),
    /*
      Where are install scripts located?
      > ./Setup
      ./Installer
      ./Install
      ./db
     */
    array(
        'question_text' => 'Where should your module\'s install scripts be located?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './Setup',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Install',
                'is_correct' => false
            ),
            array(
                'answer_text' => './db',
                'is_correct' => false
            )
        )
    ),
    /*
      What are the three event types that install scripts implement for a module that does not use composer?
      > install
      > upgrade
      > recurring
      uninstall is only for composered modules
      removal
     */
    array(
        'question_text' => 'What are the three event types that install scripts implement for a module that does not use composer?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'install',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'upgrade',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'recurring',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'uninstall',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'reinstall',
                'is_correct' => false
            )
        )
    ),
    /*
      What are the two main constructs that install scripts act upon?
      > database schema
      > database data
      database indexes
      database views
     */
    array(
        'question_text' => 'What are the two main constructs that install scripts act upon?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'database schema',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'database data',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'database indexes',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'database stored procedures',
                'is_correct' => false
            )
        )
    ),
    /*
      Where are a module's tests stored?
      > ./Test
      ./phpunti
      ./UnitTests
     */
    array(
        'question_text' => 'Where should your module\'s tests be stored?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './Test',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Phpunit',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Unit',
                'is_correct' => false
            )
        )
    ),
    /*
      Code in ./Ui modifies what?
      > Ui components
      views
      javascript code
      phtml
     */
    array(
        'question_text' => 'Code in ./Ui modifies what?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'Ui components',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Views',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Javascript code',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'templates',
                'is_correct' => false
            )
        )
    ),
    /*
      What file types can be found in ./view/(area)/web?
      > js
      > css
      > phtml
      php
     */
    array(
        'question_text' => 'What file types can be found in ./view/(area)/web?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'javascript',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'css',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'phtml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'php',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'sql',
                'is_correct' => false
            )
        )
    ),
    /*
      What files types are likely to be found in ./view/(area)/web?
      php
      > js
      > css
      > scss
      > less
      xml
      > images
      json
     */
    array(
        'question_text' => 'What files types are likely to be found in ./view/(area)/web?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'php',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'js',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'css',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'scss',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'less',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'images',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'json',
                'is_correct' => false
            )
        )
    ),
    /*
      What is stored in ./view/(area)/web/template
      > KnockoutJS html templates
      phtml files
      js templates
     */
    array(
        'question_text' => 'What is stored in ./view/(area)/web/template?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'KnockoutJS html templates',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'phtml files',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'js templates',
                'is_correct' => false
            )
        )
    ),
    /*
      How many modules can use one route/frontName?
      just one
      > multiple
     */
    array(
        'question_text' => 'How many modules can use one route/frontName?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'one',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'multiple',
                'is_correct' => true
            )
        )
    ),
    /*
      How do you specify what theme your theme extends?
      > <parent> tag in ./theme.xml
      <extends> tag in ./theme.xml
      add a parameter in ComponentRegistrar::register() in./registration.php

     */
    array(
        'question_text' => 'How do you specify what theme your theme extends?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => '&lt;parent&gt; tag in ./theme.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => '&lt;extends&gt; tag in ./theme.xml',
                'is_correct' => false
            ),
             array(
                'answer_text' => 'as a parameter in ComponentRegistrar::register()',
                'is_correct' => false
            ),
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
