<?php

$qaData = array(
    array(
        'question_text' => 'By default, blocks are injected with a ',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\View\Element\Template\Context',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\View\App\Context',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Concrete blocks extend ',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\View\Element\AbstractBlock',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\View\App\Context',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What public function returns a block\'s markup?',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'toHtml',
                'is_correct' => true
            ),
            array(
                'answer_text' => '_toHtml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'render',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can intercept the output for every block in the system with ',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'a plugin for Magento\Framework\View\Element\AbstractBlock::toHtml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'injecting a modifield Magento\Framework\View\Element\Render',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these functions is called earliest in a block\'s rendering process?',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'toHtml',
                'is_correct' => true
            ),
            array(
                'answer_text' => '_beforeToHtml',
                'is_correct' => false
            ),
            array(
                'answer_text' => '_toHtml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these functions is called latest in a block\'s rendering process?',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'toHtml',
                'is_correct' => false
            ),
            array(
                'answer_text' => '_afterToHtml',
                'is_correct' => false
            ),
            array(
                'answer_text' => '_toHtml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'view_block_abstract_to_html_after is dispatched',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What happens when toHtml is called on a block and a cache entry exists?',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'the block returns the value of the cache entry',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the cache takes over execution',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What happens when toHtml is called on a block and a cache entry does not exist?',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'the block\'s _toHtml method parses the template',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the cache takes over execution',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A block can be cached when ',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'block_html cache is enabled',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the block\'s cache lifetime is not null',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'A block\'s cache lifetime is set to 1 day by default',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A block\'s cache lifetime is not set by default.',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A block will not be cached by block cache if ___ returns false or null.',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'getCacheLifetime',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'getIsCacheable',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You must explicitly set a block\'s cache lifetime to allow it to be cached.',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When generating a block cache entry, session id parameters are replaced with a placeholder',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When generating a block cache entry, session id parameters are removed.',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A template of type vendor/magento/framework/View/Element/Template.php does what?',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'renders html',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'prints a text string',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'returns the output of its child blocks',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A template of type vendor/magento/framework/View/Element/Text.php does what?',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'renders html',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'prints a text string',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'returns the output of its child blocks',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A template of type (vendor/magento/framework/View/Element/Text/ListText.php does what?',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'renders html',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'prints a text string',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'returns the output of its child blocks',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'With M2\'s addition of containers to layout, use cases for non template blocks are frequent.',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'With M2\'s addition of containers to layout, use cases for non template blocks are few.',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The easiest way to hole punch full page cache to avoid caching just one block while letting FPC continue to function for the rest of the page is ',
        'study_guide_section' => '3.2',
        'answers' => array(
            array(
                'answer_text' => 'to load that block\'s content via AJAX',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'to set cacheable="false" on the block',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
