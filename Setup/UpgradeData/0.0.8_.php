<?php

$qaData = array(
    array(
        'question_text' => 'How could you modify how price is rendered?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => 'create catalog_product_prices.xml, reference block render.product.prices, and pass custom arguments',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'some other thing',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How would you render price on a page?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => 'create a block of type Magento\Catalog\Pricing\Render and pass required value for price_type_code',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'some other thing',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => './view/(area)/layout/catalog_product_prices.xml controls what?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => 'how prices are rendered by specifying rendering classes and templates',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'how prices are calculated by specifying discounts based on customer groups',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the correct hierarchy for product pricing data?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => 'product type > price collection > price pool > prices',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'product type > price pool > price collection > prices',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'product type > price pool > prices > single price',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What interface must all products implement in order to have a price/prices?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => 'Pricing\SaleableInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Catalog\SellableInterface',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Data\ProductInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What class encapsulates a product\'s prices and adjustments?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Pricing\PriceInfo',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Pricing\PriceData',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\Data\PriceInfo',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What product function returns an object containing all the product\'s prices and adjustments?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Catalog\Model\Product->getPriceInfo',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Pricing\Product->getPriceInfo',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\Data\Model\Product->getPriceInfo',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'At what point in the price rendering process are the class/interface names representing various prices converted to displayable numbers?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => 'in the template file',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'in the render block constructor',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'in the Magento\Framework\Pricing\Render instance',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The classes representing prices for default product types live where?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Catalog\Pricing\Price\\',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Checkout\Pricing\Price\\',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Magento\Framework\Pricing\Price',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A module creates a new product type called Flammable, which has a price called FlammablePrice. Where is the FlammalePrice class most likely to live?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => './Pricing/Price/FlammablePrice',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Pricing/FlammablePrice',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Price/Flammable',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Price/FlammablePrice',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A product of type default corresponds to what product type in admin?',
        'study_guide_section' => '7.2',
        'answers' => array(
            array(
                'answer_text' => 'simple',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'bundle',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'virtual',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What URL would access your module\'s class at ./Controller/Widget/Display.php, given the following' .
        ' xml lives at ./etc/routes.xml? ' . PHP_EOL . PHP_EOL .
        '<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:App/etc/routes.xsd">
    <router id="standard">' . PHP_EOL .
        '<route id="mywidgets" frontName="widget">' . PHP_EOL .
            '<module name="Yourcompany_Widget" />' . PHP_EOL .
        '</route>' . PHP_EOL .
    '</router>' . PHP_EOL .
'</config>' . PHP_EOL,
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'http://example.com/widget/widget/display',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'http://example.com/mywidgets/widget/display',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'http://example.com/mywidgets//display',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the primary method that a controller must implement?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'route()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'execute()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'run()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'control()',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
