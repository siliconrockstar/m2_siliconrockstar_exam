<?php

$qaData = array(
    array(
        'question_text' => 'You have created a custom theme. You want to customize the template at ./view/frontend/templates/cart.phtml in the' .
            ' Magento Checkout module. What file do you create in your theme?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './Magento_Checkout/templates/cart.phtml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './templates/cart.phtml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './templates/override/cart.phtml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './templates/magento_checkout_cart.phtml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In which file are plugins declared?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './etc/di.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/plugin.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/plugin_class_name.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/observer.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In which file are observers declared?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './etc/events.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/observers.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/listeners.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Fill in the blank: A final class or method ___ be targeted by a plugin',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'cannot',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'can',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'will',
                'is_correct' => false
            )
            
        )
    ),
    array(
        'question_text' => 'Which of these are NOT valid plugin method prefixes?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'while',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'before',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'after',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'around',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'save',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What config file would you edit in your module to add a new shipping method?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './etc/config.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/carriers.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/module.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You want to create a shipping method/carrier. What abstract class should you extend?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Shipping\Model\Carrier\AbstractCarrier',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Shipping\Model\Method\AbstractMethod',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Magento\Shipping\Model\Method\Yourmom',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You want to create a shipping method/carrier. What interface should you implement?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Shipping\Model\Carrier\CarrierInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Shipping\Model\Method\ShippingMethodInterface',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Magento\Shipping\Model\Method\YourmomInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which are the basic UI components in Magento that form the basis for all secondary UI components?',
        'study_guide_section' => '3',
        'answers' => array(
            array(
                'answer_text' => 'Form component',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Listing component',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Grid component',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Input component',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which module implments UI components?',
        'study_guide_section' => '3',
        'answers' => array(
            array(
                'answer_text' => 'Magento_UI',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento_Components',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Framework_Frontend',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Framework_UI',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What are the possible components of a UI Component?',
        'study_guide_section' => '3',
        'answers' => array(
            array(
                'answer_text' => 'XML declaration to specify configuration',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'JavaScript class inheriting from one of the Magento JavaScript framework UI componene base classes',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'KnockoutJS HTML templates',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Framework_UI',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'optional PHP modifiers',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'phtml files',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The base xml declaration for all UI Components lives in what file?',
        'study_guide_section' => '3',
        'answers' => array(
            array(
                'answer_text' => 'Magento/Ui/view/base/ui_component/etc/definition.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento/Ui/etc/ui_component.xml.',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Where do you declare UI component XML in your module?',
        'study_guide_section' => '3',
        'answers' => array(
            array(
                'answer_text' => './view/(area)/ui_component/(component name).xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/ui_component.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/ui_component/definition.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/ui/(component_name).xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Extension developers ___ introduce new components. Developers ___ customize existing ones.',
        'study_guide_section' => '3',
        'answers' => array(
            array(
                'answer_text' => 'cannot, can',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'can, cannot',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'can, can',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'cannot, cannot',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What file would you create in your module to customize the UI Component declared by Magento_Catalog at ./view/adminhtml/ui_component/category_form.xml?',
        'study_guide_section' => '3',
        'answers' => array(
            array(
                'answer_text' => './view/adminhtml/ui_component/category_form.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/adminhtml/Magento_Catalog/ui_component/category_form.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Magento_Catalog/ui_component/category_form.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Category attributes ___ automatically displayed in admin in M2.',
        'study_guide_section' => '3',
        'answers' => array(
            array(
                'answer_text' => 'are not',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'are',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What file would you create in your module to customize the display layout of URL http://example.com/customer/account/login?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './view/frontend/layout/customer_account_login.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/adminhtml//customer_account_login.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './view/layout/login.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './view/layout/frontend/customer_login.xml',
                'is_correct' => false
            )
        )
    ),
);

$this->qaData = array_merge($this->qaData, $qaData);
