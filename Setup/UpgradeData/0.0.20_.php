<?php

$qaData = array(
    array(
        'question_text' => 'Where are themes stored?',
        'study_guide_section' => '3.1',
        'answers' => array(
            array(
                'answer_text' => './app/design/(area)',
                'is_correct' => true
            ),
            array(
                'answer_text' => './app/theme/(area)',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which files are required for a theme?',
        'study_guide_section' => '3.1',
        'answers' => array(
            array(
                'answer_text' => './registration.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => './theme.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './theme.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Overriding a Magento module template can be accomplished by ',
        'study_guide_section' => '3.1',
        'answers' => array(
            array(
                'answer_text' => 'replicating the directory structure in your own theme',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'injecting your theme\'s template via di.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'pointing Magento to your theme file in layout',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which themes does M2 ship with by default? ',
        'study_guide_section' => '3.1',
        'answers' => array(
            array(
                'answer_text' => 'Blank',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Luma',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Base',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Default',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which CSS compiler does Magento support by default?',
        'study_guide_section' => '3.1',
        'answers' => array(
            array(
                'answer_text' => 'LESS',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'SASS',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which theme file is required only if the theme has no parent?',
        'study_guide_section' => '3.1',
        'answers' => array(
            array(
                'answer_text' => './etc/view.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './theme.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './registration.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Aside from the arguments passed, registration.php is identical for both modules and themes.',
        'study_guide_section' => '3.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can turn on template hints using the Magento binary',
        'study_guide_section' => '3.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the Magento binary command for enabling template hints?',
        'study_guide_section' => '3.1',
        'answers' => array(
            array(
                'answer_text' => 'dev:template-hints:enable',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'dev:hints:enable',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What are the steps to override a core or 3rd party template in your theme?',
        'study_guide_section' => '3.1',
        'answers' => array(
            array(
                'answer_text' => 'create a directory named like the module that contains the template you want to override, e.g. Company_Module',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'recreate the target module file structure, starting within ./templates, and ending with the template file',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'edit layout to pass a template argument to the block',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
