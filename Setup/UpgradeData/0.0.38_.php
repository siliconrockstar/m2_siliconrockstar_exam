<?php

$qaData = array(
    array(
        'question_text' => 'Order ___ is designed to be modified to fit fulfillment flows more readily than order ___',
        'study_guide_section' => '9.1',
        'answers' => array(
            array(
                'answer_text' => 'status, state',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'state, status',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'By design, multiple order ___ can point to a single order ___.',
        'study_guide_section' => '9.1',
        'answers' => array(
            array(
                'answer_text' => 'statuses, state',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'states, status',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Order states are hardwired into \Magento\Sales\Model\Order',
        'study_guide_section' => '9.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Order statuses are hardwired into \Magento\Sales\Model\Order',
        'study_guide_section' => '9.1',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Invoices are built against ',
        'study_guide_section' => '9.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Sales\Api\Data\InvoiceInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Sales\Api\InvoiceInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Invoices support extension attributes.',
        'study_guide_section' => '9.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In admin, credit memos created from the order view screen are not automatically refunded online.',
        'study_guide_section' => '9.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In admin, credit memos created from an invoice that used an online payment method are then refunded online.',
        'study_guide_section' => '9.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In admin, it is possible to create a credit memo against an uninvoiced order.',
        'study_guide_section' => '9.1',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
