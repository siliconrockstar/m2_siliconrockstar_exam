<?php

$qaData = array(
    array(
        'question_text' => 'The "My Account" area of the customer control panel is configured with which layout file?',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => './view/frontend/layout/customer_account.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/frontend/admin/customer_account.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which are steps to add a new menu item to the customer "My Account" area?',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'create ./view/frontend/layout/customer_account.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'in layout, reference block customer_account_navigation',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'in layout, add a block of class \Magento\Customer\Block\Account\SortLinkInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'in layout, specify the label, path, and sortOrder arguments for the block you created',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'extend the \Magento\Customer\Block\Account\SortLinkInterface class',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can add a new menu item to the customer "My Account" area without touching .php or .phtml files.',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The Order History page uses which layout file?',
        'study_guide_section' => '10.1',
        'answers' => array(
            array(
                'answer_text' => './view/frontend/layout/sales_order_history.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/frontend/layout/order_history.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './view/adminhtml/layout/history.xml',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
