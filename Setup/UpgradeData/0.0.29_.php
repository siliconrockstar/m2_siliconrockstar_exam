<?php

$qaData = array(
    array(
        'question_text' => ' Admin area grids and forms are implemented using ',
        'study_guide_section' => '6.2',
        'answers' => array(
            array(
                'answer_text' => 'UI Components',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'phtml templates',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'KnocksJS and SQL',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'UI component configuration is stored as ___ and then output to the page as ___',
        'study_guide_section' => '6.2',
        'answers' => array(
            array(
                'answer_text' => 'XML, JSON',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'JSON, XML',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Admin area UI component configuration is stored at ',
        'study_guide_section' => '6.2',
        'answers' => array(
            array(
                'answer_text' => './view/adminhtml/ui_component/(component_name).xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/adminhtml/ui_component.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In order to function, UI components require ',
        'study_guide_section' => '6.2',
        'answers' => array(
            array(
                'answer_text' => 'a data source',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'XML field configuration',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'phtml files',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'json field configuration',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'UI Components are added to page layout by ',
        'study_guide_section' => '6.2',
        'answers' => array(
            array(
                'answer_text' => 'adding a <uiComponent> tag within <referenceContainer> or <referenceBlock>',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'setting the uiComponent attribute on a <block>',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Like other parts of Magento, the easiest way to build a UI component is usually to ',
        'study_guide_section' => '6.2',
        'answers' => array(
            array(
                'answer_text' => 'find a component in core and copy/paste the component config to ./view/adminhtml/ui_component/file.xml, tweak the config, and add it to page layout',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'write it from scratch',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'UI Components are added to page layout by ',
        'study_guide_section' => '6.2',
        'answers' => array(
            array(
                'answer_text' => 'adding a <uiComponent> tag within <referenceContainer> or <referenceBlock>',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'setting the uiComponent attribute on a <block>',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'By convention, UI Component data providers are stored in your module at ',
        'study_guide_section' => '6.2',
        'answers' => array(
            array(
                'answer_text' => './Ui/',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/(area)/ui_component',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Options for customizing data providers include ',
        'study_guide_section' => '6.2',
        'answers' => array(
            array(
                'answer_text' => 'a plugin to append data to getData()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'setting a preference for the provider in di.xml',
                'is_correct' => true
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
