<?php

$qaData = array(
    array(
        'question_text' => 'What file contains the autoloader used for non-composer modules?',
        'study_guide_section' => '1.1',
        'answers' => array(
            array(
                'answer_text' => '/app/ComposerAutoloader.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/app/etc/NonComposerComponentRegistration.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/app/NonComposerComponentLoader.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What class manages the list of application components?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Component\ComponentRegistrar',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Component\Registrar',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\Application\Registrar',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What file contains a list of enabled modules?',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => '/app/etc/config.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/app/etc/local.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/app/etc/env.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/app/etc/config.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Generally speaking, should the file that contains a list of modules be in version control?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => 'yes',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'no',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What parameters should be passed to \Magento\Framework\Component\ComponentRegistrar::register() to register a component?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => 'type',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'name',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'path',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'version',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What tag is used in (module)/etc/module.xml to indicate that your module has a dependency on other modules?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => '&lt;depends&gt;',
                'is_correct' => false
            ),
            array(
                'answer_text' => '&lt;after&gt;',
                'is_correct' => false
            ),
            array(
                'answer_text' => '&lt;sequence&gt;',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What file contains local configuration values, like the database settings?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => '/app/etc/local.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/app/etc/env.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/app/etc/di.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/app/etc/config.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Where is the Magento binary?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => '/bin/magento',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/bin/magerun',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/magento',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the Magento binary command if you want to see if a module is enabled?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => 'module:status',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'module:disabled',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'module:isEnabled',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the Magento binary command to enable a module?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => 'module:enable',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'module:activate',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'module:isEnabled 1',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Does disabling a module delete its database tables?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => 'no',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'yes',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Where is the main composer file?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => '/composer.json',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/composer.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/var/composer.json',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/composer.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Where are service contracts stored in your module?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => './Api',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Contracts',
                'is_correct' => false
            ),
            array(
                'answer_text' => './service.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/api.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Service contracts are primarily implemented using what PHP construct?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => 'interfaces',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'classes',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'traits',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'inheritance',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What module folder contains service contract definitions for data objects?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => './Api/Data',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Api/Objects',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Setup',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Data',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When you\'re building a module that depends on another module, you should prefer to base your implementation on ',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => 'the other module\'s interfaces',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the other module\'s concrete classes',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these places might contain Magento core modules?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => '/vendor/framework',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/var/framework',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/app/code/Magento',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Where should you put modules you are working on, provided they are do not use Composer?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => '/app/code/YourCompany',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/app/code/local/YourCompany',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/vendor/local',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/vendor/YourCompany',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento blocks are roughly equivalent to what component in the MVVM system?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => 'ViewModel',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Model',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'View',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'This is an M2.2.x development, but instead of extending a block class, it is recommended to implement custom presentation logic by using layout to pass what into the block?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => 'an <argument> named viewModel that specifies a ViewModel class',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'a <sequence> referencing another module',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'a custom template name',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'ViewModels, as they are injected into blocks, must implement what interface?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\View\Element\Block\ArgumentInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\View\Element\Block\ViewModelInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What folder in your module should contain console commands for the magento binary?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => './Console',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Binary',
                'is_correct' => false
            ),
            array(
                'answer_text' => './bin',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Cmd',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Console commands extend what class?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => '\Symfony\Component\Console\Command\Command',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Symfony\Component\Bin\Command',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Component\Console\Command',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Bin\Command',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What part of the MVC architecture routes web requests?',
        'study_guide_section' => '0',
        'answers' => array(
            array(
                'answer_text' => 'Controller',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Model',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'View',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What part of the MVC architecture routes web requests?',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => './Controller',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Model',
                'is_correct' => false
            ),
            array(
                'answer_text' => './View',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Router',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'All application requests are dispatched by what class?',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\App\FrontController',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\App\Dispatch',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\App\Router\DefaultRouter',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento routers extend what class?',
        'study_guide_section' => '1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\App\Router\Base',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\App\Router\Default',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
