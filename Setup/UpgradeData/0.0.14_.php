<?php

$qaData = array(
    array(
        'question_text' => 'What is the path to the Magento binary?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => './bin/magento',
                'is_correct' => true
            ),
            array(
                'answer_text' => './shell/magento',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The Magento 2 binary is based on ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'the Symfony Console component',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the Magento 1 binary',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Powershell',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You only need to write the first letter of a magento binary command if the command string is unambiguous.',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Typing the Magento binary command setup:di:compile is the same as typing s:d:c',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the difference between cache:flush and cache:clean?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'clean deletes Magento cache values, flush deletes the keys as well',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'flush deletes Magento cache values, clean deletes the keys as well',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the difference between cache:flush and cache:clean?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'clean deletes Magento cache values, flush purges the caching system completely, even if it contains non-Magento data',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'flush deletes Magento cache values, clean purges the caching system completely, even if it contains non-Magento data',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What command shows if the various caches are enabled or disabled?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'cache:status',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'cache:list',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'cache:show',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'cache:enabled (cache name)',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What command shows the deploy mode?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'deploy:mode:show',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'deploy:mode',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'display:mode',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What command sets the deploy mode?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'deploy:mode:set (mode)',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'deploy:mode (mode)',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'display:mode (mode)',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What are the valid arguments to set the deploy mode via cli?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'developer, default, production',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'development, default, prod',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'dev, default, prod',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What command turns on/off db query logging?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'dev:query-log:(disable|enable)',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'dev:db-data:log:(enable|disable)',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'dev:db-log',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which command triggers an reindexing of all indexes?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'indexer:reindex',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'index:reindex-all',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How do you reindex just one index?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'indexer:reindex (index_name)',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'index:reindex-one (index_name)',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'index:reindex:(index-name)',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What command do you need to install your module after you\'ve set up the required module files?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'module:enable Companyname_Modulename',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'module:install Companyname_Modulename',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'setup:enable Companyname_Modulename',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How do you install *all* modules?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'append --all to the command',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'specify no module identifier after the command',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which command shows a list of modules and their install statuses?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'module:status',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'module:list',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which command must be run to synchronize database changes after incrementing a module version?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'setup:upgrade',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'module:install',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'setup:install',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'module:upgrade',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which command will show you if you need to run setup:upgrade?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'setup:db:status',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'setup:status',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How can you see th elist of available binary commands?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'append no argument',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'append -h',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'append --help',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What binary command enables maintenance mode?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'maintenance:enable',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'mtn:enable',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'flag:enable',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What binary command enables maintenance mode?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'maintenance:enable',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'mtn:enable',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'flag:enable',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What binary command deploys static content?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'setup:static-content:deploy',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'static-content:deploy',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'static-condiments:destroy',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What binary command reads dependency injection configuration and generates classes?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'setup:di:compile',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'di:compile',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'setup:compile',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What binary command lists websites?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'store:website:list',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'sites',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'store:list',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Running commands via the magento binary is generally considered ___ secure than admin.',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'more',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'less',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Enabling modules and running setup scripts has to be done using the command line during development',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    /*
     * Enable maintenance mode
• Copy files to the deploy target
• Enable modules / apply deploy configuration
• Run dependency compilation
• Build static assets
• Upgrade the database
• Disable maintenance mode
     */
    array(
        'question_text' => 'Which of these is *not* a required step for a non-initial, non-pipeline deployment to production?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'set deploy mode to production',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'build static assets',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'run setup:upgrade to upgrade the database',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'build static assets',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In production mode, assets are symlinked into ./pub/static',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In production mode, assets are symlinked into ./pub/static',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Exceptions are displayed to the user in developer mode and default mode.',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A fresh Magento install starts in ___ mode.',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'default',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'production',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'developer',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In developer mode, static content symlinks can be refreshed by ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'the dev:static-content:deploy command',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'simply deleting the symlink',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'developer',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In production mode, Magento automatically generates proxy and factory classes on the fly.',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Your module\'s cache configuration is stored at ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => './etc/cache.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/memcached.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './cache.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A module\'s cache configuration is stored at ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => './etc/cache.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/memcached.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './cache.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which cache stores XML configuration and data from the core_config_data table?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'config',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'system',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which cache stores XML configuration and data from the core_config_data table?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'config',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'system',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'If caching is enabled, which cache should be flushed after editing system.xml?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'config',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'system',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'If caching is enabled, you may need to flush the ___ cache after editing files in ./app/design',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'layout',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'system',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The block_html cache contains ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'the output of block\'s toHtml functions',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'multi-record database records',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'the result of resource model operations',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'javascript and css',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The block_html cache contains ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'the output of block\'s toHtml functions',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'multi-record database records',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'the result of resource model operations',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'javascript and css',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The collections cache contains ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'the result of multi-row database queries',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'arrays of active record objects',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Database table structure is cached in the cache called ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'db_ddl',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'eav',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'collections',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When adding methods to API service contracts, you may need to flush the ___ cache.',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'config_webservice',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'db_ddl',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'full_page',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Full page cache can use which of the following?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'the database',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the filesystem',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'redis',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Adding cacheable="false" to a block will ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'disable full page caching for the whole page',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'disable block_html caching for the page',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Which operation is less drastic?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'cache:clean',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'cache:flush',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'By default, full page cache uses which of the following?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'the database',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'the filesystem',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'redis',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which caching mechanism is the fastest?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'the database',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'the filesystem',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'redis',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'You can hole punch (disable caching) for a single block by ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'by setting the _isScopePrivate property to true in a block class.',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'setting cacheable="false" on the block via layout',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The recommended way to hole punch Magento is to ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'serve a cached page and then use a UI component to load data asynchronously via AJAX',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'set cacheable="false" on the blocks you need to hole punch',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can clear a cache programmatically by calling ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\App\CacheInterface::remove()',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\App\CacheInterface::clear()',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Session and content cache data ___ be stored together in the same Redis instance.',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'should not',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'should',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can clean a single object from the cache by ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'dispatching a clean_cache_by_tags event and passing the object you want to clear',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'calling \Magento\Framework\Cache::cleanCacheByTag(object)',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'calling \Magento\Framework\App\CacheInterface::clean(array of tags)',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'If Magento is using the filesystem for caching, you can destroy cache with ',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'rm -rf ./var/cache',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'rm -rf ./var/tmp',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'telnet (cache ip) 3306; echo FLUSHALL;',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the preferred way to set a block\'s template via layout?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => 'referenceBlock and add and <argument> with name="template"',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'referenceBlock and use an <action> with method="setTemplate"',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which layout file would apply to every page/route on the whole site?',
        'study_guide_section' => '1.7',
        'answers' => array(
            array(
                'answer_text' => './view/base/layout/default.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/base/layout/all.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './view/frontend/layout/theme.xml',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
