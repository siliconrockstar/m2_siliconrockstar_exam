<?php

$qaData = array(
    array(
        'question_text' => 'Which two files are necessary to bootstrap a module?',
        'study_guide_section' => '1.1',
        'answers' => array(
            array(
                'answer_text' => './etc/module.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/CompanyName_ModuleName.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/registration.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => './registration.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => './module.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What additional file is required to bootstrap a composer-enabled module?',
        'study_guide_section' => '1.1',
        'answers' => array(
            array(
                'answer_text' => './etc/composer.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './composer.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './composer.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => './composer.json',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What file contains the composer autoloader used for non-composer modules?',
        'study_guide_section' => '1.1',
        'answers' => array(
            array(
                'answer_text' => '/app/ComposerAutoloader.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/app/etc/NonComposerComponentRegistrarion.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/app/NonComposerComponentLoader.php',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
