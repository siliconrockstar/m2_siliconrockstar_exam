<?php

$qaData = array(
    array(
        'question_text' => 'Custom totals models are wired up in ',
        'study_guide_section' => '8.2',
        'answers' => array(
            array(
                'answer_text' => './etc/sales.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/config.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/adminhtml/system.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Custom totals models are wired up in ',
        'study_guide_section' => '8.2',
        'answers' => array(
            array(
                'answer_text' => './etc/sales.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/config.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/adminhtml/system.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'To add an item to cart, use ',
        'study_guide_section' => '8.2',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Sales\Model\Quote::addItem',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Sales\Model\Cart::addItem',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/adminhtml/system.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A customer can use ___ coupon codes in the cart.',
        'study_guide_section' => '8.2',
        'answers' => array(
            array(
                'answer_text' => 'only one',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'multiple',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Though its a rare use case, you can built a cart ',
        'study_guide_section' => '8.2',
        'answers' => array(
            array(
                'answer_text' => 'through ./Setup/UpgradeData.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'through ./Setup/InstallSchema.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => '___ apply to products in the cart. ___ apply to products before being added to the cart.',
        'study_guide_section' => '8.3',
        'answers' => array(
            array(
                'answer_text' => 'Sales rules, Catalog rules',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Catalog rules, Sales rules',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => '___ have more options than ___.',
        'study_guide_section' => '8.3',
        'answers' => array(
            array(
                'answer_text' => 'Sales rules, Catalog rules',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Catalog rules, Sales rules',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Sales rules affect performance negatively. This can be mitigated by ',
        'study_guide_section' => '8.3',
        'answers' => array(
            array(
                'answer_text' => 'limiting the sales rule scope to as small a customer group or number of websites as possible',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'uh.... something else',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Sales rules ___ add another product to the cart.',
        'study_guide_section' => '8.3',
        'answers' => array(
            array(
                'answer_text' => 'can',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'cannot',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What construct is basically a wrapper for a customer\'s shopping cart session?',
        'study_guide_section' => '8.1',
        'answers' => array(
            array(
                'answer_text' => 'quote',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'quote item',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'order',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Quote addresses also store a list of items assocaited with the address.',
        'study_guide_section' => '8.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which event is dispatched when a product is added to the cart via the product page, wishlist page, during reorder, and during quote merge?',
        'study_guide_section' => '8.3',
        'answers' => array(
            array(
                'answer_text' => 'sales_quote_add_item',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'sales_quote_product_add_after',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'checkout_cart_product_add_after',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'checkout_cart_add_item',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can customize how an item is rendered in the cart by ',
        'study_guide_section' => '8.3',
        'answers' => array(
            array(
                'answer_text' => 'modifying cart item renderers that live in various modules at ./view/frontend/layout/checkout_cart_item_renderers.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'some other thing',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which are things you can do on teh cart page?',
        'study_guide_section' => '8.3',
        'answers' => array(
            array(
                'answer_text' => 'change item quantity',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'delete item',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'edit item',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'move item to wishlist (if enabled)',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'add gift message (if enabled)',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'apply / remove coupon',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'go to checkout',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'register new account',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'From the cart page, clicking "edit" on a product takes you to ',
        'study_guide_section' => '8.3',
        'answers' => array(
            array(
                'answer_text' => 'the product page',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'the product edit page, at a URL starting with /checkout/cart/configure/',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'From the cart page, clicking "edit" on a product accesses which controller?',
        'study_guide_section' => '8.3',
        'answers' => array(
            array(
                'answer_text' => 'vendor/magento/module-checkout/Controller/Cart/Configure.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'vendor/magento/module-catalog/Controller/Product/View.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How could you have your module delete a product from the cart when another product is deleted?',
        'study_guide_section' => '8.3',
        'answers' => array(
            array(
                'answer_text' => 'hook in to sales_quote_remove_item',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'a plugin for \Magento\Quote\Model\Quote::removeItem OR \Magento\Quote\Api\CartItemRepositoryInterface::deleteById',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'You can modify checkout by creating the layout file ',
        'study_guide_section' => '8.3',
        'answers' => array(
            array(
                'answer_text' => './view/frontend/layout/checkout_index_index.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/adminhtml/layout/checkout_index_index.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './view/frontend/layout/checkout.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which are steps to adding a field to shipping address?',
        'study_guide_section' => '8.3',
        'answers' => array(
            array(
                'answer_text' => 'use an installer to add a column to sales_order_address',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'create layout file checkout_index_index.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'copy/paste <item name="shipping-address-fieldset"...> and add a child node',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'create an extension attribute for address model',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
