<?php

$qaData = array(
    array(
        'question_text' => 'Repositories encapsulate ',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'CRUD operations',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'caching',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'javascript',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Repositories are build on top of collections.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Collections are preferred over repositories for most database operations.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Repositories are build against interfaces.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'To get a single object from the database with a repository, you should use what method?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'getById()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'load()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'select()',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'To get multiple objects from the database with a repository, you should use what method?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'getList()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'load()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'getCollection()',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which class helps you search via a repository?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Api\SearchCriteriaBuilder',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Search\Api\SearchCriteriaBuilder',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\Api\SearchParams',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is teh general process for searching/filtering records with a resource?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'call getList on the repository while passing in a \Magento\Framework\Api\SearchCriteria',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'call getCollection on the repository after specifying parameters using addFilter()',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the correct scope hierarchy for repository filtering constructs, from smallest to largest?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'filter > filter group > filter processor',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'filter > filter processor > filter group',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'filter processor > filter group > filter',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Resource models all extend what class?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'vendor/magento/framework/Model/ResourceModel/Db/AbstractDb.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'vendor/magento/Db/Model/ResourceModel/Db/AbstractDb.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which event occurs first when a resource model is loaded.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'The resource model’s beforeLoad method is called',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'The load select is generated.',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'The row is fetched.',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which event occurs last when a resource model is loaded.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'The object is returned',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'The load select is generated.',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'The row is fetched.',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which event occurs first when a resource is saved?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'A db transaction is started',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'The resource model\'s beforeSave method is called',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'The row is fetched.',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When a resource model is saved, ___ after the db transaction is committed.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'the *_save_after_commit event is dispatched',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'something else',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'durrr... something',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which are options for appending extra data to an object?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'extend the class and use your new class in its place',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'create a new table and model and relate it to the original object table at the db level',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'use an extension attribute',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Which are options for appending extra data to an object?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'extend the class and use your new class in its place',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'create a new table and model and relate it to the original object table at the db level',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'use an extension attribute',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'use an attribute',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Extension attributes support ACLs.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Entities must extend ___ to be compatible with extension attributes.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Model\AbstractExtensibleModel',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Data\Model\AbstractExtensible',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What method returns the extension attributes associated with an entity?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'getExtensionAttributes',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'getExtensions',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'getData',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Extension attribute data is saved to the database automatically.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Extension attribute data is saved to the database ___. Extension attribute data is loaded from the database ___.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'manually, manually',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'automatically, automatically',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'manually, automatically',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'automatically, manually',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Extension attributes are configured in ',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => './etc/extension_attributes.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/data.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Model/Extension/(ClassName).php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The getExtensionAttributes function returns ',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'the objects associated extension attribute objects',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'an automatically generated interface consisting of getters/setters based on the extension attributes config file',
                'is_correct' => true
            )
        )
    ),
    /* 
     * How to create an xtn att
     * 
     * specify the extended class, extension attribute code, and extension attribute interface in ./etc/extension_attributes.xml
     * add getter and setter functions to the interface
     * create an object that implements that interface
     * create plugins to get and save teh extension attribute data on teh extended class
     *  afterSave
     *  afterGet
     *  afterGetList
     * 
     * The entity’s getExtensionAttributes method returns an auto-generated interface that
     */
    array(
        'question_text' => 'When creating an extension attribute, you must ',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'specify the target class you are extending with an extension attribute in the extension attributes xml file',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'specify a code and interface for the extension attribute in the extension attributes xml file',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'add getter and setter functions to the interface you defined',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'create plugins around the extended class to load and save the extension attribute data',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'The public methods for collections that existed in M1 no longer exist in M2.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which method(s) would help you filter a collection?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'addFieldToFilter',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'addAttributeToFilter',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'addSearchParameter',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which method(s) would help you sort a collection?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'addOrder',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'addAttributeToFilter',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'addSearchParameter',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which method(s) add a column to the returned collection?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'addFieldToSelect',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'addColumnToSelect',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'addSearchParameter',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which method(s) would change the amount of items returned in a collection?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'setPageSize',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'limit',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'setLimit',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Generally speaking, the naming convention for repositories is to ',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'append "Repository" to the base business object name.',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'prepend "Repository" to the base business object name.',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'not specified.',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these are common repository functions?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'save',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'getById',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'getList',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'delete',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'deleteById',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'All repository interfaces must, by design, implement the basic repository functions to save, get, delete, etc.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'There is nothing in the system to force a repository to implement the basic repository functions to save, get, delete, etc.',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The preferred method to save an object is to ',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'pass the object as a parameter into a repository\'s save() method',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'call the object\'s save() method',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which construct is passed into a repository\'s method to filter the collection of returned objects?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Api\SearchCriteriaInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Api\ListBuilder',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which is the correct hierarchy, from smallest to largest, for search constructs?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'filter > filter group > search criteria object',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'search criteria object > filter > filter group',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which Magento\Framework\Api\Filter function is used to filter a field for a value?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'setData(field|value|condition_type, value)',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'createFilter(field|value|condition_type, value)',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'addParam(field|value|condition_type, value)',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which class is designed to make creating a search filter group easier and less error prone (by omitting repeated calls to setData, among other things)?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Framework\Api\FilterGroupBuilder',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Framework\Api\FilterBuilder',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Search filters within the same group are added to search critiria using a logical OR - filter A is true OR filter B is true OR...',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Search filter groups are added to search critiria using a logical AND - filter group A is true AND filter B is true AND...',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which SearchCriteriaBuilder method is a shortcut for building filter groups that contain a single filter?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'addFilter(field, value, condition_type)',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'addSimpleFilterGroup(field, value, condition_type)',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento-specific database exceptions are thrown in ',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'vendor/magento/framework/Model/ResourceModel/Db/AbstractDb.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'vendor/magento/Data/Model/ResourceModel/Db/AbstractDb.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which error is thrown if you try to create a table with a primary key that already exists?',
        'study_guide_section' => '4.1',
        'answers' => array(
            array(
                'answer_text' => 'Unique constraint violation found',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Empty main table name',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Foreign key error',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Recurring type setup scripts are triggered ',
        'study_guide_section' => '4.2',
        'answers' => array(
            array(
                'answer_text' => 'every time that setup:upgrade is run whether or not an install or upgrade happened.',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'every time that setup:upgrade is run if an install or upgrade happened.',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these are valid setup scripts?',
        'study_guide_section' => '4.2',
        'answers' => array(
            array(
                'answer_text' => 'InstallData.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'InstallSchema.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'UpgradeData.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'UpgradeSchema',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Recurring.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'ReinstallSchema.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'ReinstallData.php',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
