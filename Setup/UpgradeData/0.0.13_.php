<?php

$qaData = array(
    array(
        'question_text' => 'Event observers ___ modify the objects pass to it.',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'should not',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'should',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => '___ should be used to modify the input or output of a function.',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'plugins',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'observers',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Global event observers are configured in your module at ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => './etc/events.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/obeservers.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/config.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Do configure observers for only a certain area, ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'put events.xml in a subdirectory of the area name, like ./etc/frontend/events.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'set teh value of the area attribute for the observer',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'configured the observer as a child of an <area> tag in events.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Event observer classes must implement ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Event\ObserverInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Event\Observer\ObserverInterface',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\Observer\EventInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'An observer class\' main method is ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'execute()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'observe()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'main()',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'An observer class\' main method is passed ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'a \Magento\Framework\Event\Observer',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'a \Magento\Event\Observer',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'a \Magento\Framework\Observer\Event',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A single event is dispatched by  ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'a class that implements \Magento\Framework\Event\InvokerInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'a \Magento\Framework\Event\Dispatcher',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'a \Magento\Framework\Observer\Event',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The stack of event callbacks is recursively dispatched by  ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Event\Manager',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Event\Dispatcher',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\Event\InvokerDefault',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Cron jobs are configured in a file named ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'crontab.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'cron.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'jobs.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Cron jobs can be separated by area by putting their config file in a folder at ./etc/(area)/',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento cron jobs use ___ digit cron syntax.',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'five',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'six',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Cron groups are configured in ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => './etc/cron_groups.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/cron.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './cron.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/jobs.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'By default, Magento provides a cron group called ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'index',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'cron',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'indexer',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'cache',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Sensitive system config values must be set using either environment variables or which magento binary command?',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'config:sensitive:set',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'config:set',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'password:set',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Generally, the format for setting system variables by name is ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => '<SCOPE>__<SYSTEM__VARIABLE__NAME>',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<SCOPE>__<STORE>__<VARIABLE_NAME>',
                'is_correct' => false
            ),
            array(
                'answer_text' => '<VARIABLE_NAME>',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The name prefix for setting system config variables globally via environment variable is ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'CONFIG__DEFAULT__',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'GLOBAL__',
                'is_correct' => false
            ),
            array(
                'answer_text' => '__',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The name prefix for setting system config variables at the store level via environment variable is ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'CONFIG__STORES__<STORE_VIEW_CODE>__<SYSTEM__VARIABLE__NAME>',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'CONFIG__DEFAULT__<SYSTEM__VARIABLE__NAME>',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'CONFIG__<STORE_VIEW_CODE>__<SYSTEM__VARIABLE__NAME>',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'For setting a system variable value via environment variable, the environment variable name you need to use is ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'the system variables\'s configuration path, but with slashes replaced by double underscores',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the system variable\'s array keys from env.php, concatenated using commas',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What environment variable name would you use for globally setting the config value at path design/theme/theme_id?',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'CONFIG__DEFAULT__DESIGN__THEME__THEME_ID',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'GLOBAL_DESIGN_THEME_THEME_ID',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'CONFIG__DESIGN__THEME__THEME_ID',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What environment variable name would you use for setting the config value at path design/head/title_prefix for store with store view code=site1?',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'CONFIG__STORES__SITE1__DESIGN__HEAD__TITLE_PREFIX',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'CONFIG__SITE1__DESIGN__HEAD__TITLE_PREFIX',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'SITE1_DESIGN_HEAD_TITLE_PREFIX',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What environment variable name would you use for setting the config value at path design/head/title_prefix for a website with code=site1?',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'CONFIG__WEBSITES__SITE1__DESIGN__HEAD__TITLE_PREFIX',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'CONFIG__SITE1__DESIGN__HEAD__TITLE_PREFIX',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'SITE1_DESIGN_HEAD_TITLE_PREFIX',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento implements most automatically available events in what class?',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Model\AbstractModel',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Data\Model\Events',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento exposes automatically generated events when which of the following occur?',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'an object is saved',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'an object is loaded',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'an object is deleted',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Automatically generated event names are prepended with ',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'a protected variable called _eventPrefix that is set in the model',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'a public variable called _eventObject that is set in the model',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'a public static variable called _eventName that is set in the model',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When deciding to use an event or a plugin, remember that events should be able to be run ___.',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'asynchronously',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'without a bootstrapped application object',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'on a cron schedule',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'If you need to transform data, you should use a ___.',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => 'plugin',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'event',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'To dispatch an event from your class, inject a ___ and then call its ___ method.',
        'study_guide_section' => '1.6',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\Event\ManagerInterface, dispatch',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Event, fire',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\Event\Dispatch, call',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
