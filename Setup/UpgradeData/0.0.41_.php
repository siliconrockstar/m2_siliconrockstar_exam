<?php

$qaData = array(
    array(
        'question_text' => 'How would you customize the product creation form in admin?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'add product_form.xml in the <your_module_dir>/view/adminhtml/ui_component/, add new or customize existing elements',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'optionally, use a modifier class if static declaration is not sufficient.',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'drink some camel milk',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'If UI component configuration files share the same name they are merged.',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'UI component modifier classes ',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'are used when static declaration in ui component configuration xml is insufficient',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'provide dynamic configuration that is merged with the ui component configuration xml at run time',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'are injected as a virtualtype of Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\Pool in di.xml',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Tiered pricing can be configured by ',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'website',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'store view',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'quantity',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'customer group',
                'is_correct' => true
            ),
             array(
                'answer_text' => 'shipping address',
                'is_correct' => false
            ),
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
