<?php

$qaData = array(
    array(
        'question_text' => 'RequireJS manages ',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'javascript dependencies',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'loading scripts asynchronously',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'UI animation',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'RequireJS manages ',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'javascript dependencies',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'loading scripts asynchronously',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'UI animation',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento 2 no longer supports the generic <script> tag.',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Javascript can be inserted into an HTML element using ',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'data-mage-init attribute',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'script attribute',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'text/x-magento-init element',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Javascript can be inserted into the page as a whole using ',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'data-mage-init attribute',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'script attribute',
                'is_correct' => false
            ),
            array(
                'answer_text' => '<script> with type="text/x-magento-init" attribute',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What is a limitation of using <script> and jQuery\'s document.ready() to attach javascript to an element?',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'Since you are missing a call to require(), you may have broken dependencies due to script load order',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'You cannot use <script> in M2',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'You cannot use jQuery functions directly in M2',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'One of the advantages to loading session and customer data over AJAX is ',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'You can keep blocks and the page cacheable, improving performance',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'You avoid CSRF vulnerability',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A grid or form in admin or checkout can most easily be implemted with ',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'a UI Component',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'KnockoutJS and <script type="text/x-magento-init">',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'a jQuery widget',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Simple DOM manipulation is a use case best suited to',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'a UI Component',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'KnockoutJS and <script type="text/x-magento-init">',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'a jQuery widget',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => '___ initializes JS within a specific HTML element, whereas ___ initializes JS at the page level without direct element access.',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'The data-mage-init attribute, <script type="text/x-magento-init">',
                'is_correct' => false
            ),
            array(
                'answer_text' => '<script type="text/x-magento-init">, the data-mage-init attribute, ">',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
