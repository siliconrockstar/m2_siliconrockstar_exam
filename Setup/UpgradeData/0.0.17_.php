<?php

$qaData = array(
    array(
        'question_text' => 'What are your options for customizing URL structure?',
        'study_guide_section' => '2.3',
        'answers' => array(
            array(
                'answer_text' => 'URL rewrites for basic mods',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'declaring a custom router',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'creating a custom route parser',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento\'s default 404 page is a ',
        'study_guide_section' => '2.3',
        'answers' => array(
            array(
                'answer_text' => 'CMS page stored in the database',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'static page served from ./static',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'full page template stored in ./var',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Custom 404 functionality can be most easily created by ',
        'study_guide_section' => '2.3',
        'answers' => array(
            array(
                'answer_text' => 'injecting a noRouteHandler into \Magento\Framework\App\Router\NoRouteHandlerList',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'creating a custom router',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'adding a noRoute tag to <router>',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The order in which layout XML files are loaded is based on ',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => '<sequence> tags in ./etc/module.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<after> tags in di.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'alphabetization',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In M2, layout is ',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'a hierarchy of xml files that controls page display',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'what you do to get tan',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Where can layout file be found?',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => '/app/code/(Company)/(Module)/web/(area)/layout/',
                'is_correct' => true
            ),
            array(
                'answer_text' => '/app/design/(area)/(Company or Package)/(Theme)/layout/',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'All the layout XML files are ',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'merged into one XML tree and cached',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'lazy loaded and parsed individually',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which class handles reconciling the multiple layout files?',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\View\Model\Layout\Merge',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Layout\Load',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these are valid ways to debug layout problems?',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'hook in to layout_generate_blocks_after event and print the layout for the page',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'check if your file is being loaded at \Magento\Framework\View\Model\Layout\Merge::loadFileLayoutUpdatesXml()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'check for errors in logs or on the frontend if in developer mode',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What function handles block html rendering?',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\View\Element\AbstractBlock::toHtml',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\View\Element\AbstractBlock::renderHtml',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\View\Block\AbstractBlock::toHtml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What interface defines the function to render block html?',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\View\Element\BlockInterface',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\View\Element\AbstractBlock',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\View\BlockInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What functions can a template access in its associated block class?',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'only public functions',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'public and protected functions',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'public, protected, and private functions',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What variable is automatically exposed that lets you access a block from a template?',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => '$block',
                'is_correct' => true
            ),
            array(
                'answer_text' => '$parent',
                'is_correct' => false
            ),
            array(
                'answer_text' => '$data',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Calling var_dump($this) in a phtml template file returns a ',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Framework\View\TemplateEngine\Php',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Framework\View\Template',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'data object accessible via self::getData',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento 2 renders html using output buffering and include calls.',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento 2 renders html using streams and require calls.',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Why trying to target layout changes to a specific page, you can usually determine what to name your layout file by ',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'looking at the class attribute on the body tag in browser',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'checking the layout handles rendered in system log',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can access an array of the current page\'s layout handles using ',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\View\Layout\ProcessorInterface::getHandles',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\View\Layout::getHandles',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A page request\'s full action name is the same as the layout file name you need to use to target that page.',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A page request\'s full action name is the same as the layout file name you need to use to target that page.',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these can you use to target a layout node?',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'referenceContainer',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'referenceBlock',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'block',
                'is_correct' => false
            )
            ,
            array(
                'answer_text' => 'container',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Layout file scope can be restricted by area',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A layout file in ./web/frontend/layout/ ',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'will not be parsed for admin area requests',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'will only be parsed for admin area requests',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which layout files use the Magento ACL system to determine if they should be displayed?',
        'study_guide_section' => '2.4',
        'answers' => array(
            array(
                'answer_text' => 'adminhtml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'frontend',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'both frontend and adminhtml',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
