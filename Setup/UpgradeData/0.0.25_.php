<?php

$qaData = array(
    array(
        'question_text' => 'EAV improves ___ at the expense of ___',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'flexibility, performance',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'performance, flexibility',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'EAV stands for ',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'Entity Attribute Value',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Earl Always Vogues',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Eat All Vogons',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The EAV system stores the primary key in a table named like ',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => '*_entity',
                'is_correct' => true
            ),
            array(
                'answer_text' => '*_pk',
                'is_correct' => false
            ),
            array(
                'answer_text' => '*_constraint',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The EAV system stores attributes in a table named like ',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => '*_attribute',
                'is_correct' => true
            ),
            array(
                'answer_text' => '*_value',
                'is_correct' => false
            ),
            array(
                'answer_text' => '*_(datatype)',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What attribute data determines the table its value is stored in?',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'backend_type',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'datatype',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'entity',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'EAV values are stored in tables named like ',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'eav_entity_(entity\'s datatype)',
                'is_correct' => true
            ),
            array(
                'answer_text' => '_value_(entity\'s datatype)',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'EAV attributes of type "static" are stored where?',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'the parent _entity table',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'a eav_entity_(entity\'s datatype) table',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Redis/filesystem, depending on cache setup',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What happens when a new attribute is added to the system?',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'A row is added to eav_entity_attribute',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'If the attribute is added programatically, and no attribute set is defined, the attribute is added to all attribute sets',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'If the attribute is added via admin, the attribute is added to all attribute sets',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Attribute sets ',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'determine what fields appear when editing a product record',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'are useful for when you have multiple categories of highly different products',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'group attributes on an edit page to make them easier to find',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Attribute groups ',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'determine what fields appear when editing a product record',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'are useful for when you have multiple categories of highly different products',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'group attributes on an edit page to make them easier to find',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Which are useful for creating customizations based on changes to attribute values?',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'create a plugin on afterSave on the entity',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'use a custom backend model if the attribute is installed programmatically',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'set up mview.xml and listen for changes to an attribute\'s table',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'EAV data is stored ___; flat table data is stored ___.',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'vertically, horizontally',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'horizontally, vertically',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Database data that is highly variable, where the primary object has varying number of data columns, is a possible use case for ',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'the EAV pattern',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'materialized views',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The EAV pattern should be preferred over flat tables when possible.',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The EAV pattern adds a layer of complexity over flat tables that makes development more time-consuming.',
        'study_guide_section' => '5.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
