<?php

$qaData = array(
    array(
        'question_text' => 'The biggest difference between the admin area and customer frontend is ',
        'study_guide_section' => '6.1',
        'answers' => array(
            array(
                'answer_text' => 'the admin area provides a context manager that utilizes ACLs for granular access control',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the frontend provides a context manager that utilizes ACLs for granular access control',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'the admin area uses a separate built in webserver',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'To which areas of the application can the ACL system be applied?',
        'study_guide_section' => '6.1',
        'answers' => array(
            array(
                'answer_text' => 'admin',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'soap api',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'rest api',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'front end',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'cron',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Secret keys in URLs protect against ',
        'study_guide_section' => '6.1',
        'answers' => array(
            array(
                'answer_text' => 'CSRF',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'XSS',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'URL secret keys exist in which areas?',
        'study_guide_section' => '6.1',
        'answers' => array(
            array(
                'answer_text' => 'adminhtml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'frontend',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'api_soap',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'api_rest',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which areas allow enforced password rotation?',
        'study_guide_section' => '6.1',
        'answers' => array(
            array(
                'answer_text' => 'adminhtml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'frontend',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'api_soap',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'api_rest',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Access Control Lists are configured at ',
        'study_guide_section' => '6.1',
        'answers' => array(
            array(
                'answer_text' => './etc/acl.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/adminhtml/acl.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'ACLs can be used to restrict extension attributes.',
        'study_guide_section' => '6.1',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The default admin area block model is ',
        'study_guide_section' => '6.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Backend\Block\Template',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\View\Block\Template',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Backend controllers should extend ',
        'study_guide_section' => '6.1',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Backend\App\AbstractAction',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\App\Action\Action',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You check is a user has permission to access a backend controller by ',
        'study_guide_section' => '6.1',
        'answers' => array(
            array(
                'answer_text' => 'overriding the _isAllowed method.',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'extending \Magento\Backend\App\AbstractAction, setting constant ADMIN_RESOURCE equal to an acl resource id, and calling parent::__construct ',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'ACL resoure id attribute values are usually in the format',
        'study_guide_section' => '6.1',
        'answers' => array(
            array(
                'answer_text' => 'Company_Module::resourceName',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\path\to\resource\classOrInterface.php',
                'is_correct' => false
            )
        )
    ),
);

$this->qaData = array_merge($this->qaData, $qaData);
