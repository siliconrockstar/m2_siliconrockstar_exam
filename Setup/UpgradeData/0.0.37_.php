<?php

$qaData = array(
    array(
        'question_text' => '___ provide a list of ___',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => 'Carriers, rates',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Rates, carriers',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Shippers, options',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What steps are required to add the admin controls for a new shipping carrier?',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => 'Create a new group in etc/adminhtml/system.xml for carriers/[shipping_code]',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Create a new group in etc/adminhtml/config.xml for carriers/[shipping_code]',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Create the path for default/ carriers[shipping_code] with default values in ./etc/config.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Create the path for default/ carriers[shipping_code] with default values in ./etc/system.xml',
                'is_correct' => false
            ),
        )
    ),
    array(
        'question_text' => 'Carriers should implement ___ and possibly extend ___.',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Shipping\Model\Carrier\CarrierInterface, \Magento\Shipping\Model\Carrier\AbstractCarrierOnline',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Carrier\Model\Carrier\CarrierInterface, \Magento\Carrier\Model\Carrier\AbstractCarrierOnline',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Shipping\Model\Carrier\AbstractCarrierOnline, \Magento\Shipping\Model\Carrier\CarrierInterface',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The main function that carriers use to generate rates is ',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Shipping\Model\Carrier\AbstractCarrierInterface::collectRates',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Shipping\Model\Carrier\AbstractCarrier::generateRates',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Retrieving rates from a third party system is a use case for',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => 'API rates',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'fixed rates',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'table rates',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Retrieving rates from a third party system is a use case for a carrier that uses ',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => 'API rates',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'fixed rate shipping',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'table rates',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Applying the same shipping fee regardless of cart total or location is a use case for ',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => 'API rates',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'fixed rate shipping',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'table rates',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Calculating shipping by referencing a static document that contains zip codes and weights is a use case for ',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => 'API rates',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'fixed rate shipping',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'table rates',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can test if a carrier is enabled by checking ',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Shipping\Model\Carrier\AbstractCarrierOnline::canCollectRates',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Shipping\Model\Carrier\AbstractCarrierOnline::isEnabled',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can test if a simple payment method is enabled by checking that ___ returns ___.',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Payment\Model\Method\AbstractMethod::isAvailable(), true',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Payment\Model\Method\AbstractMethod::isDisabled(), false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The module at ./vendor/magento/module-vault provides ',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => 'a PCI compliant way for customers to store CC information via Braintree',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'secure password storage for admin users',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'For troubleshooting, most payment methods have ',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => 'a verbose logging option that prints to var/log',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'secure password storage for admin users',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these are offline payment methods?',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => 'having a customer service rep call a customer and run their credit card',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'check/money order',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'bank transfer',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Paypal',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these are online payment methods?',
        'study_guide_section' => '8.4',
        'answers' => array(
            array(
                'answer_text' => 'Paypal',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'check/money order',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'bank transfer',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Authorize.net',
                'is_correct' => true
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
