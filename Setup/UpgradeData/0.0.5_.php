<?php

$qaData = array(
    array(
        'question_text' => 'Magento expects to find admin/backend controllers in what directory in your module?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './Controller/Adminhtml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './AdminController',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Controllers',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Controller/Admin',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What module directory contains cron job classes?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './Cron',
                'is_correct' => true
            ),
            array(
                'answer_text' => './Crontab',
                'is_correct' => false
            ),
            array(
                'answer_text' => './Model/Cron',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What directory should contain your module\'s configuration files?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './etc',
                'is_correct' => true
            ),
            array(
                'answer_text' => './config',
                'is_correct' => false
            ),
            array(
                'answer_text' => './xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './view',
                'is_correct' => false
            )
        )
    ),
    /*
      Which customization method does M2 generally prefer?
      > dependency injection
      composition
      inheritance
     */
    array(
        'question_text' => 'Which customization method does M2 generally prefer?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'dependency injection',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'composition',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'inheritance',
                'is_correct' => false
            )
        )
    ),
    /*
      Which are valid configuration files that could be found in your module's ./etc/ ?
      cron.xml
      > crontab.xml
      > webapi.xml
      > di.xml
      injection.xml
      > config.xml
      api.xml
      > acl.xml
     */
    array(
        'question_text' => 'Which are valid configuration files that could be found in your module\'s ./etc/ directory?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'cron.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'crontab.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'webapi.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'di.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'injection.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'config.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'api.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'acl.xml',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'How would you apply a module\'s config file to only one area?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'create a subfolder in ./etc',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'use an <area> tag',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'create an admin route',
                'is_correct' => false
            )
        )
    ),
    /*
      Where in your module should translation files live?
      > ./i18n
      ./translations
      ./etc/language
     */
    array(
        'question_text' => 'Where in your module should translation files live?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => './i18n',
                'is_correct' => true
            ),
            array(
                'answer_text' => './translations',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/language',
                'is_correct' => false
            )
        )
    ),
    /*
      What file format does M2 use for translation files?
      json
      xml
      php
      > csv
     */
    array(
        'question_text' => 'What file format does M2 use for translation files?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'csv',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'json',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'php',
                'is_correct' => false
            )
        )
    ),
    /*
      What kind of model holds CRUD operations?
      crud
      > resource
      databse
      collection
     */
    array(
        'question_text' => 'What kind of model holds CRUD operations?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'resource',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'crud',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'database',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'collection',
                'is_correct' => false
            )
        )
    ),
    /*
      What is M2's preferred way to write a new database record?
      > create a repository and have it save your model's data
      create a collection and have it save your model's data
      create a new model and save it directly
     */
    array(
        'question_text' => 'What is M2\'s preferred way to write a new database record?',
        'study_guide_section' => '',
        'answers' => array(
            array(
                'answer_text' => 'create a repository and have it save your model\'s data',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'create a collection and call save()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'create a new model and save it directly',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
