<?php

$qaData = array(
    array(
        'question_text' => 'Magento 2 prefers ___ over ___',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'Composition, Inheritance',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Inheritance, Composition',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is Dependency Injection?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'Giving a class what it needs to function when it is instantiated',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Overriding and merging xml files into one configuration object',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What construct manages object storage and instantiation?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\ObjectManager\ObjectManager',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\Data\ObjectManager',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Framework\Injection',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'The Mage:: god class',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How does M2 inject dependencies?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'via __construct()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'via __set()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'via __load()',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'It is ___ to access the ObjectManager directly.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'generally bad practice',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'best practice',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which are situations where you might need to access the ObjectManager directly?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'static methods like __wakeup() and __sleep()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'in global scope, like fixtures in unit tests',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'in classes that create other objects, e.g. factories or proxies',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'By default, constructor arguments in M2 are passed ___.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'by reference',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'by value',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Objects contained within M2\s DI system are, by default, ___ objects.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'singleton',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'transient',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What does a proxy do in M2?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'Operates as a stand in until a dependency is needed, letting you lazy-load the class',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Operates as a translator between objects',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Proxies and factories are ___.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'Automatically generated',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Manually generated',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Proxies should be ___.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'Passed into __construct()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Specified in di.xml',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'You can tell Magento to generate a factory for a class by ___.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'Appending "Factory" to the class name, like \Company\Module\Api\Data\ThingFactory',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Specifying factory availability in di.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can tell Magento to use a proxy for a class by ___.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'Appending "Proxy" to the class name in di.xml, like \Company\Module\Api\Data\Thing\Proxy',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Add a proxy="true" tag in di.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Passing the proxy class into the constructor',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'M2\'s generated factories are used to create ___.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'other transient classes.',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'other singleton classes',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What one public method does a generated M2 factory class have?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'create()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'execute()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'instantiate()',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can manually create factory classes yourself.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When does class instantiation occur?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'at the time of injection',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'when a factory\'s create() method is called',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'on database i/o',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'after cache is flushed',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Where does the ObjectManager look to locate the proper class to instantiate for an interface?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'in a combination of all the di.xml files',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'in the config object',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'in the database',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'M2 uses a different ObjectManager\Factory\Dynamic\(Class).php based on deploy mode.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What factory class does the ObjectManager use to create classes in developer mode?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Framework\ObjectManager\Factory\Dynamic\Developer',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Framework\ObjectManager\FactoryInterface',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Magento\Framework\ObjectManager\Factory\Dynamic',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What factory class does the ObjectManager use to create classes in production mode?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Framework\ObjectManager\Factory\Dynamic\Production',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Framework\ObjectManager\FactoryInterface',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Magento\Framework\ObjectManager\Factory\Dynamic',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'One good way to request an object that is *not* shared is ___.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'to create it via a factory',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'to inject it via di.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'to use the ObjectManager directly',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Magento stores its generated code in ___.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => '/generated/',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the database',
                'is_correct' => false
            ),
            array(
                'answer_text' => '/var/',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Specifying a plugin in di.xml allows you to ___.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'wrap another class\' public function',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'hook into the event system',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'extend a UI component',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Specifying a preference in di.xml allows you to ___.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'substitute your own class for a given class or interface',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'set admin user preferences, including session length',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'dictate the return type of a function',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Specifying a virtual type in di.xml allows you to ___.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'create an instance of an existing class with modified constructor arguments',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'extend an existing class while keeping the same constructor arguments',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Virtual types can be used to ___.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'reduce redundancy when all you need is to change a constructor for an existing class',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'extend an existing class while keeping the same constructor arguments',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'di.xml allows you to override constructor arguments for existing classes.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'One of the benefits of M2\'s DI system over M1\'s god class is it lets you',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'override a specific class with one of your own classes, but in a non-global way',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'makes great guacamole',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Plugins work on ___ methods.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'public',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'private',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'public and private',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How would you override a core class in di.xml?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'use a <preference>',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'use a <rewrite>',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'use an <argument>',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How would you insert your class into another class in di.xml?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'use a <type> to specify the class receiving the injection, and then add an <argument> with your class',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'use a <preference> to specify the class being injected',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
