<?php

$qaData = array(
    array(
        'question_text' => 'Magento has multiple root template files',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Where is the default root template located?',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => './vendor/magento/module-theme/view/base/templates/root.phtml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './vendor/magento/module-theme/view/adminhtml/templates/root.phtml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './vendor/magento/module-theme/view/frontend/templates/root.phtml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the following XML doing?' . PHP_EOL . PHP_EOL
        . '<type name="Magento\Framework\View\Result\Page">
        <arguments>
            <argument name="layoutReaderPool" xsi:type="object">pageConfigRenderPool</argument>
            <argument name="generatorPool" xsi:type="object">pageLayoutGeneratorPool</argument>
            <argument name="template" xsi:type="string">Magento_Theme::root.phtml</argument>
        </arguments>
    </type>',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => 'It\'s from module magento2-base. It\'s injecting the root template and default layout generators and renderers into the page result class',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'some other thing',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'All page layout files derive and extend ',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => 'empty.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'body.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'page.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can create a new page layout, like two-column-left.phtml, in your module, by putting a file in ',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => './view/(area)/page_layout/',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/(area)/page_layout/',
                'is_correct' => false
            ),
            array(
                'answer_text' => './view/(area)/page_layout/',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can create a new page layout, like two-column-left.phtml, in your theme, by putting a file in',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => './page_layout/',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/(area)/page_layout/',
                'is_correct' => false
            )
        )
    ),
     array(
        'question_text' => 'Where do (non-page) layout files go in a module?',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => './layout/',
                'is_correct' => false
            ),
            array(
                'answer_text' => './view/(area)/layout/',
                'is_correct' => true
            )
        )
    ),
     array(
        'question_text' => 'Where do templates go in a module?',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => './view/(area)/templates',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/(area)/template/',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Where do (non-page) layout files go in a theme?',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => './(Vendor_Module)/layout/',
                'is_correct' => false
            ),
            array(
                'answer_text' => './view/(area)/layout/',
                'is_correct' => true
            )
        )
    ),
     array(
        'question_text' => 'Where do templates go in a theme?',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => './(Vendor_Module)/templates',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/(area)/templates/',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What is the difference between a module\'s page_layout/ and layout/ directories?',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => 'page_layout/ holds layouts for the overall page structure, layout/ is for block and container layouts',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'nothing, the names are interchangeable',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What file would you create in your theme to extend the layout file at ./vendor/magento/module-catalog/view/frontend/layout/catalog_category_view.xml?',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => './Magento_Catalog/catalog_category_view.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/frontend/layout/Magento_Catalog/catalog_category_view.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What file would you create in your theme to *override* the layout file at ./vendor/magento/module-catalog/view/frontend/layout/catalog_category_view.xml?',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => './Magento_Catalog/override/catalog_category_view.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/frontend/layout/Magento_Catalog/override/catalog_category_view.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'How do you specify a page-level layout for a route?',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => 'In the route\'s layout file, wrap the XML in a <page> element and specify a value for the layout attribute',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Add a page attribute in di.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'In most cases, instead of creating a custom block, you can ',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => 'provide the template data by injecting a view model into the block via layout',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Add a page attribute in di.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Blocks are typically injected into view models.',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'The default type for a block is ',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Framework\View\Element\Template',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Framework\View\Element\Block',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'When would creating a new block (as opposed to using viewmodel injection) be necessary?',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => 'when teh template is determined at runtime, instead of being static',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'when specialized caching is required',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What steps are necessary to use a custom page layout in your module?',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => 'Put the page layout xml file at ./view/(area)/page_layout/',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Register the custom layout in ./view/frontend/layouts.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Register the custom layout in ./layouts.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Put the page layout xml file at ./page_layout/',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What steps are necessary to use a custom page layout in your theme?',
        'study_guide_section' => '2.5',
        'answers' => array(
            array(
                'answer_text' => 'Put the page layout xml file at ./page_layout/',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Register the custom layout in ./layouts.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Put the page layout xml file at ./view/(area)/page_layout/',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Register the custom layout in ./view/frontend/layouts.xml',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
