<?php

$qaData = array(
    array(
        'question_text' => 'The top level node in a ui component config file must ',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'be the name of a basic UI Component, like <form>',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'contain a link to the .xsd schema',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'reference a block',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which are common second-level nodes for a UI Component XML definition?',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => '<argument name="data" ... >',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<dataSource ... >',
                'is_correct' => true
            ),
            array(
                'answer_text' => '<fieldset ... >',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'The purpose of PHP modifiers for UI Components is ',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'to be able to modify the component\'s configuration dynamically when XML is insufficient',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'to enforce separation of concerns between UI and ViewModel Javascript',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Individual UI Component XML is ',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'merged with identically named UI Component XML files',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'merged with the base definition.xml for the component',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'UI Component DataSources ',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'are configured in the UI Component XML <argument name="dataProvider" ... > node',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'are configured in the UI Component XML <dataSource ... > node',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'provide model data to KnockoutJS ',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'provide model data to blocks and other models',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'UI Component DataSources provide data via implmenting ',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'getData()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'ajax()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'provide model data to blocks and other models',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'UI Component DataSources are implemented in the browser via ',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => 'Magento/Ui/view/base/web/js/form/provider.js',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'jQuery',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'PHP .phtml templates are stored at ___ while KnockoutJs frontend .html templates are stores at ___.',
        'study_guide_section' => '3.4',
        'answers' => array(
            array(
                'answer_text' => './view/templates, ./view/web/template',
                'is_correct' => true
            ),
            array(
                'answer_text' => './view/web/template, ./view/templates',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
