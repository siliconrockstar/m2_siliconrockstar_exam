<?php

$qaData = array(
    array(
        'question_text' => 'EAV attributes can be somewhat configured in ',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => './etc/eav_attributes.xml',
                'is_correct' => true
            ),
            array(
                'answer_text' => './etc/attributes.xml',
                'is_correct' => false
            ),
            array(
                'answer_text' => './etc/eav.xml',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which area valid values for eav_attribute.backend_type?',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => 'static',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'datetime',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'decimal',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'int',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'text',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'varchar',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'scalar',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'boolean',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'An EAV attribute that uses a yes/no control in admin would probably have eav_attribute.backend_model set to ',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => 'Magento\Catalog\Model\Product\Attribute\Backend\Boolean',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Magento\Catalog\Model\Product\Attribute\Backend\Price',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'Magento\Eav\Model\Entity\Attribute\Backend\Datetime',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A class stored in eav_attribute.backend_model ',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => 'controls how the attribute\'s value is stored in the database',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'provides a list of acceptable options for the attribute',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'A class stored in eav_attribute.backend_model ',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => 'controls how the attribute\'s value is stored in the database',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'provides a list of acceptable options for the attribute',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'The ___ table controls the majority of an EAV attribute\'s behavior and display.',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => 'eav_attribute',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'eav_entity',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'eav_entity_(datatype)',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'An EAV attribute\'s HTML form input type is stored in ',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => 'eav_attribute.frontend_input',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'eav_attribute.frontend_model',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'EAV attribute configuration uses a three part design pattern consisting of ',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => 'frontend',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'backend',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'source',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'model',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'An EAV attribute\'s backend model is a good place for ',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => 'validation',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'indexing',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'cache-related operations',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'formatting display',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'generating a list of possible values',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'To create a new EAV frontend model, you should extend ',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Eav\Model\Entity\Attribute\Frontend\AbstractBackend',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Eav\Model\Entity\Attribute\Frontend\AbstractSource',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'EAV attribute backend models extend ',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Eav\Model\Entity\Attribute\Frontend\AbstractBackend',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Eav\Model\Entity\Attribute\Frontend\AbstractSource',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'EAV attribute source models extend ',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Eav\Model\Entity\Attribute\Frontend\AbstractBackend',
                'is_correct' => false
            ),
            array(
                'answer_text' => '\Magento\Eav\Model\Entity\Attribute\Frontend\AbstractSource',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'You can specify the dropdown options for an EAV control by (pick 2) ',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => 'extending \Magento\Eav\Model\Entity\Attribute\Source\ AbstractSource',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'returning a list of options in getAllOptions method',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'specifying a list of options in layout',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can specify the dropdown options for an EAV control by (pick 1) ',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => 'creating an after plugin for teh source model\'s getAllOptions method',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'specifying a list of options in layout',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What class can you use to create EAV attributes?',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Eav\Setup\EavSetupFactory',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Eav\Setup',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Eav attributes use ___ install and upgrade scripts, not ___ install and upgrade scripts.',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => 'data, schema',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'schema, data',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Some modules have classes like \Magento\Catalog\Setup\CatalogSetup and \Magento\Customer\Setup\CustomerSetup that extend ___ in order to ___.',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Eav\Setup\EavSetupFactory, provide EAV attribute manipulation features for specific entity types',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Attribute\Setup\Attribute, provide EAV attribute manipulation features per store',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of these are installers that provide EAV manipulation features?',
        'study_guide_section' => '5.3',
        'answers' => array(
            array(
                'answer_text' => '\Magento\Catalog\Setup\CategorySetup',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Customer\Setup\CustomerSetup',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Sales\Setup\SalesSetup',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Quote\Setup\QuoteSetup',
                'is_correct' => true
            ),
            array(
                'answer_text' => '\Magento\Shipping\Setup\CarrierSetup',
                'is_correct' => false
            )
        )
    ),
);

$this->qaData = array_merge($this->qaData, $qaData);
