<?php

$qaData = array(
    array(
        'question_text' => 'Plugins work on both public and private methods.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of the following method types can be targeted by a plugin?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'public',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'private',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'protected',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'static',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of the following are valid plugin targets?',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'interfaces',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'abstract classes',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'final classes',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'private methods',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'public methods',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'final methods',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Which of the following do plugins enable a developer to do?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'modify the input of public methods',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'modify the output of public methods',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'Completely circumvent public methods',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Magento implements plugins by generating ___ classes.',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'factory',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'interceptor',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'proxy',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Plugins are executed by what class?',
        'study_guide_section' => '1.4',
        'answers' => array(
            array(
                'answer_text' => 'vendor/magento/framework/Interception/Interceptor.php',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'vendor/magento/framework/Interceptor.php',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'vendor/magento/framework/Interception/Plugin.php',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What plugin method would you use to modify the inputs for the method doGreatThing()?',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'beforeDoGreatThing()',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'doGreatThingBefore()',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'doGreatThing()',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Before plugins should return ___.',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'an array of modified arguments that have the same variable name',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the result of the wrapped method',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'null',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'If a before plugin method does not modify an argument, it should return ___ for that argument.',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'nothing, or null',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the orginal argument value',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'false for that argument',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'What happens to the result of a before plugin method?',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'It is passed to the next plugin, if existant, and eventually to the wrapped method.',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'It overrides the result of the wrapped method.',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'After plugins are used to ___.',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'modify the output of a method.',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'skip over a method.',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'After plugins return ',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'the (potentially modified) output of the wrapped method',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'null',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Around plugins ',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'give you full control over a method by fully wrapping it',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'are made of old pizza',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Around plugins ',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'increase stack traces and negatively affect performance',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'should generally be avoided if possible',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'should only be used when the execution of all further plugins and the target method needs termination',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'Around plugins are passed ',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'a callable object representing the next plugin or method in the chain',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the arguments for the original, wrapped method',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the ObjectManager instance',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'To let execution continue to the next method, around plugins must ',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'invoke the callable in $proceed',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'return a truthy value',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'do nothing special, just finish executing',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Around plugins automatically pass arguments to the callable they recieve.',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'false',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'true',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Around plugins must manually pass all the arguments required by the callable they receive.',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'If an around plugin does not call its callable, then ___.',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'the around plugin prevents execution of all the following plugins and the target function call',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'the around plugin prevents execution of all the following plugins but not the target function call',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'the following plugin and method calls continue as usual',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'magento recommends against using around plugins whenever possible.',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'true',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'false',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can control the order in which a plugin is called by setting its ___ attribute.',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'sortOrder',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'depends',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'order',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'weight',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'You can "unplug" a plugin, preventing its execution completely, by ',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'setting disabled="true" in its configuration',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'having it return immediately',
                'is_correct' => false
            )
        )
    ),
    array(
        'question_text' => 'Plugins with higher sort orders are executed ',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'earlier',
                'is_correct' => false
            ),
            array(
                'answer_text' => 'later',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'What are some of the pitfalls of using plugins?',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'An around plugin can break the whole system if it forgets to call its callable',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'They can make debugging more difficult',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'They can make reading source code harder, since a plugin may be modifying values elsewhere',
                'is_correct' => true
            )
        )
    ),
    array(
        'question_text' => 'When possible, ___ should be preferred over ___.',
        'study_guide_section' => '1.5',
        'answers' => array(
            array(
                'answer_text' => 'event observers, plugins',
                'is_correct' => true
            ),
            array(
                'answer_text' => 'plugins, event observers',
                'is_correct' => false
            )
        )
    )
);

$this->qaData = array_merge($this->qaData, $qaData);
