<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Siliconrockstar\Exam\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

    /**
     * @var array - questions => array (answers)
     */
    protected $qaData = array(
        array(
            'question_text' => 'Which of these are extensible Magento core components (select all that apply)',
            'study_guide_section' => '1.1',
            'answers' => array(
                array(
                    'answer_text' => 'Themes',
                    'is_correct' => true
                ),
                array(
                    'answer_text' => 'Language Packages',
                    'is_correct' => true
                ),
                array(
                    'answer_text' => 'Modules',
                    'is_correct' => true
                ),
                array(
                    'answer_text' => 'uQuery Widgets',
                    'is_correct' => false
                ),
                array(
                    'answer_text' => 'UI Components',
                    'is_correct' => false
                ),
                array(
                    'answer_text' => 'Routes',
                    'is_correct' => false
                ),
                array(
                    'answer_text' => 'XML Config',
                    'is_correct' => false
                ),
            )
        ),
        /* question */
        array(
            'question_text' => 'Which of these are valid Magento 2 area codes (select all that apply)',
            'study_guide_section' => '1.1',
            'answers' => array(
                array(
                    'answer_text' => 'frontend',
                    'is_correct' => true
                ),
                array(
                    'answer_text' => 'adminhtml',
                    'is_correct' => true
                ),
                array(
                    'answer_text' => 'base',
                    'is_correct' => true
                ),
                array(
                    'answer_text' => 'crontab',
                    'is_correct' => true
                ),
                array(
                    'answer_text' => 'webapi_rest',
                    'is_correct' => true
                ),
                array(
                    'answer_text' => 'webapi_soap',
                    'is_correct' => true
                ),
                array(
                    'answer_text' => 'adminarea',
                    'is_correct' => false
                ),
                array(
                    'answer_text' => 'bin',
                    'is_correct' => false
                ),
                array(
                    'answer_text' => 'cron',
                    'is_correct' => false
                ),
                array(
                    'answer_text' => 'api',
                    'is_correct' => false
                )
            )
        ),
        /* question */
        array(
            'question_text' => 'Which file is used to add entries to the admin area top menu (starting at the module root)?',
            'study_guide_section' => '6.4',
            'answers' => array(
                array(
                    'answer_text' => './etc/admin.xml',
                    'is_correct' => false
                ),
                array(
                    'answer_text' => './etc/adminhtml.xml',
                    'is_correct' => false
                ),
                array(
                    'answer_text' => './etc/adminhtml/adminhtml.xml',
                    'is_correct' => false
                ),
                array(
                    'answer_text' => './etc/adminhtml/menu.xml',
                    'is_correct' => true
                )
            )
        )
    );

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        $connection = $setup->getConnection();

        $questionTable = $setup->getTable('siliconrockstar_exam_question');
        $answerTable = $setup->getTable('siliconrockstar_exam_answer');

        /* insert questions and answers */
        foreach ($this->qaData as $q) {
            $connection->insert($questionTable, array(
                'question_text' => $q['question_text'],
                'study_guide_section' => $q['study_guide_section']
                    )
            );
            $q_id = $connection->lastInsertId();

            foreach ($q['answers'] as $a) {
                $connection->insert($answerTable, array(
                    'question_id' => $q_id,
                    'answer_text' => $a['answer_text'],
                    'is_correct' => $a['is_correct']
                        )
                );
            }
        }

        $setup->endSetup();
    }
}
