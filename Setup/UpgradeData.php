<?php

namespace Siliconrockstar\Exam\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\UserContextInterface;

/**
 * @todo uses require_once to pull in question data, kind of ghetto, but
 * not sure a better way to have giant question array files :/
 */
class UpgradeData implements \Magento\Framework\Setup\UpgradeDataInterface
{

    private $roleFactory;
    private $rulesFactory;
    private $moduleList;
    private $qaData;

    /**
     * 
     * @param \Magento\Authorization\Model\RoleFactory $roleFactory
     * @param \Magento\Authorization\Model\RulesFactory $rulesFactory
     */
    public function __construct(\Magento\Authorization\Model\RoleFactory $roleFactory, 
            \Magento\Authorization\Model\RulesFactory $rulesFactory, 
            \Magento\Framework\Module\ModuleList $moduleList) {
        $this->roleFactory = $roleFactory;
        $this->rulesFactory = $rulesFactory;
        $this->moduleList = $moduleList;
        $this->qaData = array();
    }

    /**
     * Soooo... here's a fun thing: data upgrade scripts, as designed, have no
     * upper bound. If the current version is less than the version tested for 
     * in upgrade code, that code will be run; upgrade scripts always upgrade
     * to the highest version they have code for, regardless of what version is 
     * actually set in ./etc/mdule.xml
     * 
     * Workaround below is to read the version from ./etc/module.xml and manually
     * set an upgrade limit
     * 
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {

        $setupVersion = $context->getVersion();
        
        // manually set upper bound for upgrade
        $versionLimit = $this->moduleList->getOne('Siliconrockstar_Exam')['setup_version'];

        $installer = $setup;
        $installer->startSetup();

        $files = scandir(__DIR__ . DIRECTORY_SEPARATOR . 'UpgradeData');

        /* read file names, include file if needed */
        foreach ($files as $file) {

            $match = array();
            preg_match('/(.*)\_/', $file, $match);

            if (count($match) > 0) {

                // if setup version is lower than file name version, *and* file name version is not above file limit
                if ((version_compare($context->getVersion(), $match[1]) < 0) && (version_compare($match[1], $versionLimit) <= 0)) {
                    require_once(__DIR__ . DIRECTORY_SEPARATOR . 'UpgradeData' . DIRECTORY_SEPARATOR . $file);
                }
            }
        }

        if (!empty($this->qaData)) {
            echo PHP_EOL . 'installing version ' . $setupVersion . '. Adding ' . count($this->qaData) . ' questions, it may take awhile...' . PHP_EOL;
            $this->installQuestions($setup);
        }

        $installer->endSetup();
    }

    protected function installQuestions($setup) {

        $connection = $setup->getConnection();

        $questionTable = $setup->getTable('siliconrockstar_exam_question');
        $answerTable = $setup->getTable('siliconrockstar_exam_answer');

        /* insert questions and answers */
        $count = 0;
        foreach ($this->qaData as $q) {

            $connection->insert($questionTable, array(
                'question_text' => $q['question_text'],
                'study_guide_section' => $q['study_guide_section']
                    )
            );
            $q_id = $connection->lastInsertId();

            foreach ($q['answers'] as $a) {
                $connection->insert($answerTable, array(
                    'question_id' => $q_id,
                    'answer_text' => $a['answer_text'],
                    'is_correct' => $a['is_correct']
                        )
                );
            }

            $count++;
            if ($count % 10 == 0) {
                echo $count . PHP_EOL;
            }
        }
    }

    /**
     * v 0.0.1 > v 0.0.2
     * 
     * Install admin user role
     */
    protected function installRole() {

        $role = $this->roleFactory->create();
        $role->setName('Siliconrockstar Exam Admin')
                ->setPid(0) //set parent role id of your role 
                ->setRoleType(RoleGroup::ROLE_TYPE)
                ->setUserType(UserContextInterface::USER_TYPE_ADMIN);

        $role->save();
        $resource = [
            'Magento_Backend::admin',
            'Siliconrockstar_Base::admin',
            'Siliconrockstar_Exam::admin_exam'
        ];

        /* Array of resource ids which we want to allow this role */
        $this->rulesFactory->create()->setRoleId($role->getId())->setResources($resource)->saveRel();
    }

}
