<?php
namespace Siliconrockstar\Exam\Controller\Adminhtml\Question;

class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Siliconrockstar_Exam::admin_exam';  
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/index/index');
        return $resultRedirect;
    }     
}
