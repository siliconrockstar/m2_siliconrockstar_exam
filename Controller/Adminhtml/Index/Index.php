<?php

namespace Siliconrockstar\Exam\Controller\Adminhtml\Index;

class Index extends \Magento\Backend\App\AbstractAction
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    
    const ADMIN_RESOURCE = 'Siliconrockstar_Exam::admin_exam';

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Load the page defined in view/adminhtml/layout/siliconrockstar_exam_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute() {
        return $resultPage = $this->resultPageFactory->create();
    }

}
