<?php

namespace Siliconrockstar\Exam\Controller\Index;

class Validate extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $answerCollectionFactory;
    protected $request;

    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Framework\App\RequestInterface $request, \Siliconrockstar\Exam\Model\ResourceModel\Answer\CollectionFactory $answerCollectionFactory) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->request = $request;
        $this->answerCollectionFactory = $answerCollectionFactory;
        parent::__construct($context);
    }

    public function execute() {

        $requestData = $this->getRequest()->getPost();

        $result = $this->resultJsonFactory->create();

        /** @todo move out of controller */
        /** @todo sanity check incoming post data */
        /*
         * retrieve question's answers
         *  just answer_id
         *  where correct = whatever
         *  and question_id = the question's id
         * 
         * if arrays are equal, then correct: 1, else correct: 0
         * answers: array of answer ids that are correct
         * 
         */
        if (empty($requestData['selectedAnswers']) || empty($requestData['qid'])) {
            $result->setData(array('message' => 'Please select answer(s)'));
            return $result;
        }

        $submittedAnswerIds = $requestData['selectedAnswers'];
        sort($submittedAnswerIds);
        $submittedQid = $requestData['qid'];



        $answers = $this->answerCollectionFactory->create()
                ->addFilter('question_id', $submittedQid)
                ->addFilter('is_correct', 1)
                ->addFieldToSelect('answer_id')
                ->load();

        /* array */
        $correctAnswerIds = $answers->getColumnValues('answer_id');
        sort($correctAnswerIds);

        /* if correct answer Id array = submitted answer id array, is correct; else, wrong */
        if ($correctAnswerIds == $submittedAnswerIds) {
            $message = 'Correct';
        } else {
            $message = 'INCORRECT';
        }

        $returnData = array(
            'message' => $message,
            'correctAnswerIds' => $correctAnswerIds
        );


        $result->setData($returnData);
        return $result;
    }

}
