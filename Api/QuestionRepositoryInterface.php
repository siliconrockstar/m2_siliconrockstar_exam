<?php

namespace Siliconrockstar\Exam\Api;

use Siliconrockstar\Exam\Api\Data\QuestionInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface QuestionRepositoryInterface
{

    public function save(QuestionInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(QuestionInterface $page);

    public function deleteById($id);
    
    public function getRandomQuestionId();
}
