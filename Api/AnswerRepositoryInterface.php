<?php

namespace Siliconrockstar\Exam\Api;

use Siliconrockstar\Exam\Api\Data\AnswerInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface AnswerRepositoryInterface
{

    public function save(AnswerInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(AnswerInterface $page);

    public function deleteById($id);
}
