<?php

namespace Siliconrockstar\Exam\Ui\DataProvider\Question\Form;

/**
 * Question edit form modifier. Mostly to get the select dropdown options from
 * the database.
 */
class QuestionEditModifier implements \Magento\Ui\DataProvider\Modifier\ModifierInterface
{

    protected $db;

    public function __construct(
    \Magento\Framework\App\ResourceConnection $db, \Magento\Framework\App\Helper\Context $context
    ) {
        $this->db = $db->getConnection();
    }

    public function modifyMeta(array $meta) {
        
        $formattedOptions = $this->formatOptionsArray($this->getOptions());
        
        $meta['general'] = [ # field set name
            'children' => [
                'study_guide_section' => [ # field name
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'options' => $formattedOptions,
                            ]
                        ]
                    ]
                ]
            ]
        ];
         
        return $meta;
        
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data) {
        return $data;
    }

    /**
     * Straight SQL is risky but easier and more performant here
     * 
     * @return array|null
     */
    protected function getOptions() {

        $sql = 'SELECT DISTINCT study_guide_section FROM siliconrockstar_exam_study_guide_section';
        $result = $this->db->fetchAll($sql);
        
        return array_values($result);
    }
    
    /**
     * For the meta for the UI component, options need to look like
     * 
     * array(
     *   value = 'value',
     *   label = 'label'
     * )
     * 
     * ... but in this case, value and label are the same
     * 
     * @param array $options
     */
    protected function formatOptionsArray(array $options){
        
        sort($options);

        $formattedOptions = array();
        
        foreach ($options as $option){
            $formattedOptions[] = array('value' => $option['study_guide_section'], 'label' => $option['study_guide_section']);
        }
        
        return $formattedOptions;
    }

}
