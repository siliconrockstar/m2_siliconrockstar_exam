<?php
namespace Siliconrockstar\Exam\Ui\Component\Listing\DataProviders\Siliconrockstar\Exam;

class Answer extends \Magento\Ui\DataProvider\AbstractDataProvider
{    
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Siliconrockstar\Exam\Model\ResourceModel\Answer\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }
}
