<?php

namespace Siliconrockstar\Exam\Ui\Component\Listing\DataProviders\Siliconrockstar\Exam;

class Questions extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    public function __construct(
    $name, $primaryFieldName, $requestFieldName, \Siliconrockstar\Exam\Model\ResourceModel\Question\CollectionFactory $collectionFactory, array $meta = [], array $data = []
    ) { 
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

}
