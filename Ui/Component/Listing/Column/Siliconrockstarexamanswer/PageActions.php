<?php
namespace Siliconrockstar\Exam\Ui\Component\Listing\Column\Siliconrockstarexamanswer;

class PageActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");
                $id = "X";
                if(isset($item["siliconrockstar_exam_answer_id"]))
                {
                    $id = $item["siliconrockstar_exam_answer_id"];
                }
                $item[$name]["view"] = [
                    "href"=>$this->getContext()->getUrl(
                        "siliconrockstar_exam_answer/answer/edit",["siliconrockstar_exam_answer_id"=>$id]),
                    "label"=>__("Edit")
                ];
            }
        }

        return $dataSource;
    }    
    
}
