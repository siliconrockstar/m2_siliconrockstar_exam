<?php

namespace Siliconrockstar\Exam\Ui\Component\Listing\Column\Siliconrockstarexamquestions;

class PageActions extends \Magento\Ui\Component\Listing\Columns\Column
{

    public function prepareDataSource(array $dataSource) {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");
                $id = "X";
                if (isset($item["question_id"])) {
                    $id = $item["question_id"];
                }
                $item[$name]["view"] = [
                    "href" => $this->getContext()->getUrl(
                            "siliconrockstar_exam/question/edit", ["question_id" => $id]),
                    "label" => __("Edit")
                ];
            }
        }

        return $dataSource;
    }

}
