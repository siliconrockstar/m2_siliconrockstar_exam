<?php

namespace Siliconrockstar\Exam\Block;

/**
 * @todo get off collections and their factories and use repositories
 */
class Frontend extends \Magento\Framework\View\Element\Template
{

    protected $request;
    protected $questionCollectionFactory;
    protected $answerCollectionFactory;
    protected $questionRepository;
    public $question;

    public function __construct(\Magento\Framework\App\RequestInterface $request, 
            \Siliconrockstar\Exam\Model\ResourceModel\Answer\CollectionFactory $answerCollectionFactory, 
            \Siliconrockstar\Exam\Model\ResourceModel\Question\CollectionFactory $questionCollectionFactory,
            \Siliconrockstar\Exam\Model\QuestionRepository $questionRepository,
            \Magento\Framework\View\Element\Template\Context $context, 
            array $data = []) {
        $this->request = $request;
        $this->questionCollectionFactory = $questionCollectionFactory;
        $this->answerCollectionFactory = $answerCollectionFactory;
        $this->questionRepository = $questionRepository;
        
        parent::__construct($context, $data);
    }
    
    /**
     * @todo a lot of this should be moved to a ViewModel and then injected
     * into the block via layout using attribute name="viewModel"
     */
    /**
     * grab question ID from querystring, load question with answers
     */
    function _prepareLayout() {
        $qid = $this->request->getParam('qid');

        // if no qid, just load id =1
        if (empty($qid)) {
            $qid = 1;
        }

        // get two questions at a time, to create next link
        $questionCollection = $this->questionCollectionFactory->create()
                ->addFieldToFilter('question_id', ['gteq' => $qid])
                ->setOrder('question_id', 'asc')
                ->setPageSize(2)
                ->setCurPage(1)
                ->load();

        $this->addAnswersToQuestionCollection($questionCollection);

        $questionArray = $questionCollection->toArray();
        if ($questionArray['totalRecords'] > 0) {
            $this->setData('question', $questionArray['items'][0]);
        }
        if ($questionArray['totalRecords'] > 1) {
            $this->setData('nextQuestionId', $questionArray['items'][1]['question_id']);
        }
        
        // get randome question id for random question button
        $this->setData('randomQuestionId', $this->getRandomQuestionId());
    }

    /**
     * @todo move this to a non-resourced model ExamQuestion to stitch together
     * the Question and Answer models
     * 
     * @param type $questionsCollection
     */
    protected function addAnswersToQuestionCollection($questionCollection) {
        foreach ($questionCollection->getItems() as $question) {
            $this->addAnswersToQuestion($question);
        }
    }

    protected function addAnswersToQuestion($question) {
        $qid = $question->getQuestionId();

        $answersCollection = $this->answerCollectionFactory->create()
                ->addFilter('question_id', $qid)
                ->load();

        $answers = $answersCollection->toArray();
        $answers = $answers['items'];
        shuffle($answers);
        
        $question->setAnswers($answers);
    }
    
    /*
     * return a more-or-less random question id
     */
    protected function getRandomQuestionId(){
        return $this->questionRepository->getRandomQuestionId();
    }
}
